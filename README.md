#AFAM-Microrobot
##Environment: ROS Jade
    http://wiki.ros.org/jade/Installation/Ubuntu
##Usage
###Create Catkin Workspace
    $ mkdir -p ~/catkin_ws/src
    $ cd ~/catkin_ws
    $ catkin_make
###Fork Repository
    $ git clone https://yzhnasa@bitbucket.org/nextgensystems/afam.git
###Compile Code
    $ catkin_make
###Launch RViz
    $ roslaunch afam_description display.launch
###Run AFAM driver
    $ rosrun afam_driver test_afam_node
###Show Joint Status
    $ rostopic echo /joint_states

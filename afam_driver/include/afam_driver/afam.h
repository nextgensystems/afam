#ifndef __AFAM_H__
#define __AFAM_H__

#include <memory>
#include <cmath>
#include <string>
#include <limits>
#include <boost/container/vector.hpp>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <visualization_msgs/Marker.h>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>
#include "afam_driver/solver/solver.h"

enum AFAMConstant {
    AFAM_ARM_SUPPORTER_X_STAGE_JOINT = 0,
    AFAM_ARM_SUPPORTER_Y_STAGE_JOINT = 1,
    AFAM_ARM_REVOLUTE_SUPPORTER_JOINT = 2,
    AFAM_ARM_REVOLUTE_ARM_JOINT = 3,
    AFAM_ARM_CONTROLLER_X_STAGE_JOINT = 4,
    AFAM_ARM_CONTROLLER_Y_STAGE_JOINT = 5,
    AFAM_V1_JOINT_NUMBER = 6
};

class ControlValueToCableAttachingPointKinematicEquation1 : public KinematicEquation {
    public:
        ControlValueToCableAttachingPointKinematicEquation1(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &control_value, const Eigen::VectorXd &joint_and_cable_attaching_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double x1 = control_value(0);
            double y1 = control_value(1);
            double x2 = control_value(2);
            double y2 = control_value(3);
            //double zr = driving_values(4);

            double xr = joint_and_cable_attaching_coordinate(0);
            double yr = joint_and_cable_attaching_coordinate(1);

            double xb = joint_and_cable_attaching_coordinate(2);
            double yb = joint_and_cable_attaching_coordinate(3);
            double zb = joint_and_cable_attaching_coordinate(4);
            return (xb-(x1+xc1_))*(xb-(x1+xc1_)) + (yb-(-y1+yc1_))*(yb-(-y1+yc1_)) + zb*zb - l0_*l0_;
        }
};

class ControlValueToCableAttachingPointKinematicEquation2 : public KinematicEquation {
    public:
        ControlValueToCableAttachingPointKinematicEquation2(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &control_value, const Eigen::VectorXd &joint_and_cable_attaching_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double x1 = control_value(0);
            double y1 = control_value(1);
            double x2 = control_value(2);
            double y2 = control_value(3);

            double xr = joint_and_cable_attaching_coordinate(0);
            double yr = joint_and_cable_attaching_coordinate(1);

            double xb = joint_and_cable_attaching_coordinate(2);
            double yb = joint_and_cable_attaching_coordinate(3);
            double zb = joint_and_cable_attaching_coordinate(4);
            return (xb-xr)*(xb-xr) + (yb-yr)*(yb-yr) + (zb-h0_)*(zb-h0_) - b0_*b0_;
        }
};

class ControlValueToCableAttachingPointKinematicEquation3 : public KinematicEquation {
    public:
        ControlValueToCableAttachingPointKinematicEquation3(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &control_value, const Eigen::VectorXd &joint_and_cable_attaching_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double x1 = control_value(0);
            double y1 = control_value(1);
            double x2 = control_value(2);
            double y2 = control_value(3);

            double xr = joint_and_cable_attaching_coordinate(0);
            double yr = joint_and_cable_attaching_coordinate(1);

            double xb = joint_and_cable_attaching_coordinate(2);
            double yb = joint_and_cable_attaching_coordinate(3);
            double zb = joint_and_cable_attaching_coordinate(4);
            return (xr-(x2+xc2_))*(xr-(x2+xc2_)) + (yr-(y2+yc2_))*(yr-(y2+yc2_)) - d0_*d0_;
        }
};

class ControlValueToCableAttachingPointKinematicEquation4 : public KinematicEquation {
    public:
        ControlValueToCableAttachingPointKinematicEquation4(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &control_value, const Eigen::VectorXd &joint_and_cable_attaching_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double x1 = control_value(0);
            double y1 = control_value(1);
            double x2 = control_value(2);
            double y2 = control_value(3);

            double xr = joint_and_cable_attaching_coordinate(0);
            double yr = joint_and_cable_attaching_coordinate(1);

            double xb = joint_and_cable_attaching_coordinate(2);
            double yb = joint_and_cable_attaching_coordinate(3);
            double zb = joint_and_cable_attaching_coordinate(4);
            return (xr-(x2+xc2_))*(yb-yr) - (xb-xr)*(yr-(y2+yc2_));
        }
};

class ControlValueToCableAttachingPointLagrangianAuxiliaryEquation1 : public LagrangianAuxiliaryEquation {
    public:
        ControlValueToCableAttachingPointLagrangianAuxiliaryEquation1(Eigen::VectorXd &constants) : LagrangianAuxiliaryEquation(constants) {}
        const double operator()(const Eigen::VectorXd &control_value, const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &lambda) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double x1 = control_value(0);
            double y1 = control_value(1);
            double x2 = control_value(2);
            double y2 = control_value(3);

            double xr = joint_and_cable_attaching_coordinate(0);
            double yr = joint_and_cable_attaching_coordinate(1);

            double xb = joint_and_cable_attaching_coordinate(2);
            double yb = joint_and_cable_attaching_coordinate(3);
            double zb = joint_and_cable_attaching_coordinate(4);

            double lambda1 = lambda(0);
            double lambda2 = lambda(1);
            double lambda3 = lambda(2);
            double lambda4 = lambda(3);

            return 1 - lambda1*(2*(xb-(x1+xc1_))) - lambda2*(2*(xb-xr)) + lambda4*(yr-(y2+yc2_));
        }
};

class ControlValueToCableAttachingPointLagrangianAuxiliaryEquation2 : public LagrangianAuxiliaryEquation {
    public:
        ControlValueToCableAttachingPointLagrangianAuxiliaryEquation2(Eigen::VectorXd &constants) : LagrangianAuxiliaryEquation(constants) {}
        const double operator()(const Eigen::VectorXd &control_value, const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &lambda) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double x1 = control_value(0);
            double y1 = control_value(1);
            double x2 = control_value(2);
            double y2 = control_value(3);

            double xr = joint_and_cable_attaching_coordinate(0);
            double yr = joint_and_cable_attaching_coordinate(1);

            double xb = joint_and_cable_attaching_coordinate(2);
            double yb = joint_and_cable_attaching_coordinate(3);
            double zb = joint_and_cable_attaching_coordinate(4);

            double lambda1 = lambda(0);
            double lambda2 = lambda(1);
            double lambda3 = lambda(2);
            double lambda4 = lambda(3);

            return lambda1*(2*(yb-(-y1+yc1_))) + lambda2*(2*(yb-yr)) + lambda4*(xr-(x2+xc2_));
        }
};

class ControlValueToCableAttachingPointLagrangianAuxiliaryEquation3 : public LagrangianAuxiliaryEquation {
    public:
        ControlValueToCableAttachingPointLagrangianAuxiliaryEquation3(Eigen::VectorXd &constants) : LagrangianAuxiliaryEquation(constants) {}
        const double operator()(const Eigen::VectorXd &control_value, const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &lambda) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double x1 = control_value(0);
            double y1 = control_value(1);
            double x2 = control_value(2);
            double y2 = control_value(3);

            double xr = joint_and_cable_attaching_coordinate(0);
            double yr = joint_and_cable_attaching_coordinate(1);

            double xb = joint_and_cable_attaching_coordinate(2);
            double yb = joint_and_cable_attaching_coordinate(3);
            double zb = joint_and_cable_attaching_coordinate(4);

            double lambda1 = lambda(0);
            double lambda2 = lambda(1);
            double lambda3 = lambda(2);
            double lambda4 = lambda(3);

            return lambda1*(2*zb) + lambda2*(2*(zb-h0_));
        }
};

class ControlValueToCableAttachingPointLagrangianAuxiliaryEquation4 : public LagrangianAuxiliaryEquation {
    public:
        ControlValueToCableAttachingPointLagrangianAuxiliaryEquation4(Eigen::VectorXd &constants) : LagrangianAuxiliaryEquation(constants) {}
        const double operator()(const Eigen::VectorXd &control_value, const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &lambda) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double x1 = control_value(0);
            double y1 = control_value(1);
            double x2 = control_value(2);
            double y2 = control_value(3);

            double xr = joint_and_cable_attaching_coordinate(0);
            double yr = joint_and_cable_attaching_coordinate(1);

            double xb = joint_and_cable_attaching_coordinate(2);
            double yb = joint_and_cable_attaching_coordinate(3);
            double zb = joint_and_cable_attaching_coordinate(4);

            double lambda1 = lambda(0);
            double lambda2 = lambda(1);
            double lambda3 = lambda(2);
            double lambda4 = lambda(3);

            return lambda2*(2*(xb-xr)) - lambda3*(2*(xr-(x2+xc2_))) - lambda4*((yb-yr)-(yr-(y2+yc2_)));
        }
};

class ControlValueToCableAttachingPointLagrangianAuxiliaryEquation5 : public LagrangianAuxiliaryEquation {
    public:
        ControlValueToCableAttachingPointLagrangianAuxiliaryEquation5(Eigen::VectorXd &constants) : LagrangianAuxiliaryEquation(constants) {}
        const double operator()(const Eigen::VectorXd &control_value, const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &lambda) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double x1 = control_value(0);
            double y1 = control_value(1);
            double x2 = control_value(2);
            double y2 = control_value(3);

            double xr = joint_and_cable_attaching_coordinate(0);
            double yr = joint_and_cable_attaching_coordinate(1);

            double xb = joint_and_cable_attaching_coordinate(2);
            double yb = joint_and_cable_attaching_coordinate(3);
            double zb = joint_and_cable_attaching_coordinate(4);

            double lambda1 = lambda(0);
            double lambda2 = lambda(1);
            double lambda3 = lambda(2);
            double lambda4 = lambda(3);

            return lambda2*(2*(yb-yr)) - lambda3*(2*(yr-(y2+yc2_))) + lambda4*((xr-(x2+xc2_))+(xb-xr));
        }
};

class JointToTipKinematicEquation1 : public KinematicEquation {
    public:
        JointToTipKinematicEquation1(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double xr = joint_and_cable_attaching_coordinate(0);
            double yr = joint_and_cable_attaching_coordinate(1);

            double xb = joint_and_cable_attaching_coordinate(2);
            double yb = joint_and_cable_attaching_coordinate(3);
            double zb = joint_and_cable_attaching_coordinate(4);

            double xt = end_and_tip_coordinate(0);
            double yt = end_and_tip_coordinate(1);
            double zt = end_and_tip_coordinate(2);

            double x = end_and_tip_coordinate(3);
            double y = end_and_tip_coordinate(4);
            double z = end_and_tip_coordinate(5);

            return xt - xr - (a0_ / b0_) * (xb - xr);
        }
};

class JointToTipKinematicEquation2 : public KinematicEquation {
    public:
        JointToTipKinematicEquation2(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double xr = joint_and_cable_attaching_coordinate(0);
            double yr = joint_and_cable_attaching_coordinate(1);

            double xb = joint_and_cable_attaching_coordinate(2);
            double yb = joint_and_cable_attaching_coordinate(3);
            double zb = joint_and_cable_attaching_coordinate(4);

            double xt = end_and_tip_coordinate(0);
            double yt = end_and_tip_coordinate(1);
            double zt = end_and_tip_coordinate(2);

            double x = end_and_tip_coordinate(3);
            double y = end_and_tip_coordinate(4);
            double z = end_and_tip_coordinate(5);

            return yt - yr - (a0_ / b0_) * (yb - yr);
        }
};

class JointToTipKinematicEquation3 : public KinematicEquation {
    public:
        JointToTipKinematicEquation3(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double xr = joint_and_cable_attaching_coordinate(0);
            double yr = joint_and_cable_attaching_coordinate(1);

            double xb = joint_and_cable_attaching_coordinate(2);
            double yb = joint_and_cable_attaching_coordinate(3);
            double zb = joint_and_cable_attaching_coordinate(4);

            double xt = end_and_tip_coordinate(0);
            double yt = end_and_tip_coordinate(1);
            double zt = end_and_tip_coordinate(2);

            double x = end_and_tip_coordinate(3);
            double y = end_and_tip_coordinate(4);
            double z = end_and_tip_coordinate(5);

            return zt - h0_ - (a0_ / b0_) * (zb - h0_);
        }
};

class JointToTipKinematicEquation4 : public KinematicEquation {
    public:
        JointToTipKinematicEquation4(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double xr = joint_and_cable_attaching_coordinate(0);
            double yr = joint_and_cable_attaching_coordinate(1);

            double xb = joint_and_cable_attaching_coordinate(2);
            double yb = joint_and_cable_attaching_coordinate(3);
            double zb = joint_and_cable_attaching_coordinate(4);

            double xt = end_and_tip_coordinate(0);
            double yt = end_and_tip_coordinate(1);
            double zt = end_and_tip_coordinate(2);

            double x = end_and_tip_coordinate(3);
            double y = end_and_tip_coordinate(4);
            double z = end_and_tip_coordinate(5);

            return (xb - xr) * (x - xt) + (yb - yr) * (y - yt) + (zb - h0_) * (z - zt);
        }
};

class JointToTipKinematicEquation5 : public KinematicEquation {
    public:
        JointToTipKinematicEquation5(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double xr = joint_and_cable_attaching_coordinate(0);
            double yr = joint_and_cable_attaching_coordinate(1);

            double xb = joint_and_cable_attaching_coordinate(2);
            double yb = joint_and_cable_attaching_coordinate(3);
            double zb = joint_and_cable_attaching_coordinate(4);

            double xt = end_and_tip_coordinate(0);
            double yt = end_and_tip_coordinate(1);
            double zt = end_and_tip_coordinate(2);

            double x = end_and_tip_coordinate(3);
            double y = end_and_tip_coordinate(4);
            double z = end_and_tip_coordinate(5);

            return (xb - xr) * (y - yt) - (x - xt) * (yb - yr);
        }
};

class JointToTipKinematicEquation6 : public KinematicEquation {
    public:
        JointToTipKinematicEquation6(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double xr = joint_and_cable_attaching_coordinate(0);
            double yr = joint_and_cable_attaching_coordinate(1);

            double xb = joint_and_cable_attaching_coordinate(2);
            double yb = joint_and_cable_attaching_coordinate(3);
            double zb = joint_and_cable_attaching_coordinate(4);

            double xt = end_and_tip_coordinate(0);
            double yt = end_and_tip_coordinate(1);
            double zt = end_and_tip_coordinate(2);

            double x = end_and_tip_coordinate(3);
            double y = end_and_tip_coordinate(4);
            double z = end_and_tip_coordinate(5);

            return (x - xb) * (x - xb) + (y - yb) * (y - yb) + (z - zb) * (z - zb) - (a0_ - b0_) * (a0_ - b0_) - c0_ * c0_;
        }
};

class JointToTipKinematicEquationInv1 : public KinematicEquation {
    public:
        JointToTipKinematicEquationInv1(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double xr = joint_cable_attaching_end_coordinate(0);
            double yr = joint_cable_attaching_end_coordinate(1);

            double xb = joint_cable_attaching_end_coordinate(2);
            double yb = joint_cable_attaching_end_coordinate(3);
            double zb = joint_cable_attaching_end_coordinate(4);

            double xt = joint_cable_attaching_end_coordinate(5);
            double yt = joint_cable_attaching_end_coordinate(6);
            double zt = joint_cable_attaching_end_coordinate(7);

            double x = tip_coordinate(0);
            double y = tip_coordinate(1);
            double z = tip_coordinate(2);

            return xt - xr - (a0_ / b0_) * (xb - xr);
        }
};

class JointToTipKinematicEquationInv2 : public KinematicEquation {
    public:
        JointToTipKinematicEquationInv2(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double xr = joint_cable_attaching_end_coordinate(0);
            double yr = joint_cable_attaching_end_coordinate(1);

            double xb = joint_cable_attaching_end_coordinate(2);
            double yb = joint_cable_attaching_end_coordinate(3);
            double zb = joint_cable_attaching_end_coordinate(4);

            double xt = joint_cable_attaching_end_coordinate(5);
            double yt = joint_cable_attaching_end_coordinate(6);
            double zt = joint_cable_attaching_end_coordinate(7);

            double x = tip_coordinate(0);
            double y = tip_coordinate(1);
            double z = tip_coordinate(2);

            return yt - yr - (a0_ / b0_) * (yb - yr);
        }
};

class JointToTipKinematicEquationInv3 : public KinematicEquation {
    public:
        JointToTipKinematicEquationInv3(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double xr = joint_cable_attaching_end_coordinate(0);
            double yr = joint_cable_attaching_end_coordinate(1);

            double xb = joint_cable_attaching_end_coordinate(2);
            double yb = joint_cable_attaching_end_coordinate(3);
            double zb = joint_cable_attaching_end_coordinate(4);

            double xt = joint_cable_attaching_end_coordinate(5);
            double yt = joint_cable_attaching_end_coordinate(6);
            double zt = joint_cable_attaching_end_coordinate(7);

            double x = tip_coordinate(0);
            double y = tip_coordinate(1);
            double z = tip_coordinate(2);

            return zt - h0_ - (a0_ / b0_) * (zb - h0_);
        }
};

class JointToTipKinematicEquationInv4 : public KinematicEquation {
    public:
        JointToTipKinematicEquationInv4(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double xr = joint_cable_attaching_end_coordinate(0);
            double yr = joint_cable_attaching_end_coordinate(1);

            double xb = joint_cable_attaching_end_coordinate(2);
            double yb = joint_cable_attaching_end_coordinate(3);
            double zb = joint_cable_attaching_end_coordinate(4);

            double xt = joint_cable_attaching_end_coordinate(5);
            double yt = joint_cable_attaching_end_coordinate(6);
            double zt = joint_cable_attaching_end_coordinate(7);

            double x = tip_coordinate(0);
            double y = tip_coordinate(1);
            double z = tip_coordinate(2);

            return (xb - xr) * (x - xt) + (yb - yr) * (y - yt) + (zb - h0_) * (z - zt);
        }
};

class JointToTipKinematicEquationInv5 : public KinematicEquation {
    public:
        JointToTipKinematicEquationInv5(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double xr = joint_cable_attaching_end_coordinate(0);
            double yr = joint_cable_attaching_end_coordinate(1);

            double xb = joint_cable_attaching_end_coordinate(2);
            double yb = joint_cable_attaching_end_coordinate(3);
            double zb = joint_cable_attaching_end_coordinate(4);

            double xt = joint_cable_attaching_end_coordinate(5);
            double yt = joint_cable_attaching_end_coordinate(6);
            double zt = joint_cable_attaching_end_coordinate(7);

            double x = tip_coordinate(0);
            double y = tip_coordinate(1);
            double z = tip_coordinate(2);

            return (xb - xr) * (y - yt) - (x - xt) * (yb - yr);
        }
};

class JointToTipKinematicEquationInv6 : public KinematicEquation {
    public:
        JointToTipKinematicEquationInv6(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double xr = joint_cable_attaching_end_coordinate(0);
            double yr = joint_cable_attaching_end_coordinate(1);

            double xb = joint_cable_attaching_end_coordinate(2);
            double yb = joint_cable_attaching_end_coordinate(3);
            double zb = joint_cable_attaching_end_coordinate(4);

            double xt = joint_cable_attaching_end_coordinate(5);
            double yt = joint_cable_attaching_end_coordinate(6);
            double zt = joint_cable_attaching_end_coordinate(7);

            double x = tip_coordinate(0);
            double y = tip_coordinate(1);
            double z = tip_coordinate(2);

            return (x - xb) * (x - xb) + (y - yb) * (y - yb) + (z - zb) * (z - zb) - (a0_ - b0_) * (a0_ - b0_) - c0_ * c0_;
        }
};

class JointToTipKinematicEquationInv7 : public KinematicEquation {
    public:
        JointToTipKinematicEquationInv7(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double xr = joint_cable_attaching_end_coordinate(0);
            double yr = joint_cable_attaching_end_coordinate(1);

            double xb = joint_cable_attaching_end_coordinate(2);
            double yb = joint_cable_attaching_end_coordinate(3);
            double zb = joint_cable_attaching_end_coordinate(4);

            double xt = joint_cable_attaching_end_coordinate(5);
            double yt = joint_cable_attaching_end_coordinate(6);
            double zt = joint_cable_attaching_end_coordinate(7);

            double x = tip_coordinate(0);
            double y = tip_coordinate(1);
            double z = tip_coordinate(2);

            return (x-xr)*(x-xr) + (y-yr)*(y-yr) + (z-h0_)*(z-h0_) - c0_*c0_ - a0_*a0_;
        }
};

class JointToTipKinematicEquationInv8 : public KinematicEquation {
    public:
        JointToTipKinematicEquationInv8(Eigen::VectorXd &constants) : KinematicEquation(constants) {}
        const double operator()(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
            double a0_ = constants_(0);
            double b0_ = constants_(1); // the length from where cable attach to the spring.
            double c0_ = constants_(2);
            double d0_ = constants_(3);
            double h0_ = constants_(4);
            double l0_ = constants_(5);
            double xc1_ = constants_(6);
            double yc1_ = constants_(7);
            double xc2_ = constants_(8);
            double yc2_ = constants_(9);

            double xr = joint_cable_attaching_end_coordinate(0);
            double yr = joint_cable_attaching_end_coordinate(1);

            double xb = joint_cable_attaching_end_coordinate(2);
            double yb = joint_cable_attaching_end_coordinate(3);
            double zb = joint_cable_attaching_end_coordinate(4);

            double xt = joint_cable_attaching_end_coordinate(5);
            double yt = joint_cable_attaching_end_coordinate(6);
            double zt = joint_cable_attaching_end_coordinate(7);

            double x = tip_coordinate(0);
            double y = tip_coordinate(1);
            double z = tip_coordinate(2);

            return (xt-xr)*(y-yt) - (x-xt)*(yt-yr);
        }
};

class AFAM {
    private:
        double a0_;
        double b0_; // the length from where cable attach to the spring.
        double c0_;
        double d0_;
        double h0_;
        double l0_;
        double xc1_;
        double yc1_;
        double xc2_;
        double yc2_;

        std::string afam_name_;

        Eigen::Vector4d current_control_value_;    // x1, y1
        Eigen::Vector3d current_revolute_arm_joint_coordinate_;        // xr, yr, zr=h0
        Eigen::Vector3d current_cable_attaching_coordinate_;           // xb, yb, zb
        Eigen::Vector3d current_end_coordinate_;                       // xt, yt, zt
        Eigen::Vector3d current_tip_coordinate_;                       // x, y, z
        Eigen::Vector4d lambda_;

        ConstrainedNewtonSolver solver_constrained_control_;
        UnconstrainedNewtonSolver solver_unconstrained_tip_;
        UnconstrainedNewtonSolver solver_unconstrained_tip_inv_;

        GradientSolver gradient_solver_inv_;

        ros::Publisher afam_joint_publisher_;
        ros::Publisher marker_publisher_;
        ros::Subscriber afam_joint_subscriber_;
        ros::NodeHandle node_handle_;
        sensor_msgs::JointState state_msg_;
        visualization_msgs::Marker points_;
        geometry_msgs::Point point_msg_;

        void updateTrajectory(const Eigen::VectorXd &tip_coordinate);
        void updateJointState(const Eigen::VectorXd &joint_state);
        void updateView(const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &joint_state);
    public:
        AFAM(int &argc, char **argv, const std::string &afam_name);
        AFAM(int &argc, char **argv, const std::string &afam_name, const double a0, const double b0, const double c0, const double d0, const double h0, const double l0, const double xc1, const double yc1, const double xc2, const double yc2);
        AFAM(int &argc, char **argv, const std::string &afam_name, Eigen::VectorXd &constants);
        void setParameters(Eigen::VectorXd &constants);
        Eigen::Vector4d &calculateControlValue(Eigen::Vector3d &next_tip_coordinate);
        Eigen::Vector3d &calculateTipCoordinate(Eigen::Vector4d &next_control_value);
        Eigen::Vector4d &movingTipToNextCoordinate(Eigen::Vector3d &next_tip_coordinate);
        Eigen::Vector3d &movingTipToNextCoordinate(const Eigen::Vector4d &next_control_value);
        //void movingTipToNextCoordinate(Eigen::Vector3d &next_tip_coordinate);
        Eigen::Vector3d &getCurrentTipCoordinate();
        std::string getName();
        void setPointColor(double alpha, double r, double g, double b);
        void resetState();
};

class ObjectiveFunctionAFAM : public ObjectiveFunction {
    AFAM *afam_;
    Eigen::Vector3d next_tip_coordinate_;
public:
    ObjectiveFunctionAFAM(AFAM *afam, Eigen::Vector3d &next_tip_coordinate) : afam_(afam), next_tip_coordinate_(next_tip_coordinate), ObjectiveFunction() {}
    const double operator()(const Eigen::VectorXd &control_value){
        return (next_tip_coordinate_-afam_->movingTipToNextCoordinate(control_value)).norm();
    }
};

#endif /* __AFAM_H__ */

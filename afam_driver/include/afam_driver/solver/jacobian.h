#ifndef __JACOBIAN_H__
#define __JACOBIAN_H__

#include <iostream>
#include <cstddef>
#include <memory>
#include <limits>
#include <boost/container/vector.hpp>
#include <eigen3/Eigen/Dense>
#include "afam_driver/solver/equation.h"

class Jacobian {
    private:
        boost::container::vector<EquationPtr> equations_ptrs_;
        boost::container::vector<std::shared_ptr<Equation>> equations_;
    protected:
        const unsigned int max_iteration_;
        const double error_tolerance_;
        const double initial_step_size_;

        const double partialDerivative(const EquationPtr equation_ptr, const unsigned int partial_variable_index, const Eigen::VectorXd &abscissa_value);
        const double partialDerivative(std::shared_ptr<Equation> &equation, const unsigned int partial_variable_index, const Eigen::VectorXd &abscissa_value);
    public:
        Jacobian();
        Jacobian(const boost::container::vector<EquationPtr> &equations_ptrs);
        Jacobian(const boost::container::vector<std::shared_ptr<Equation>> &equations);
        Jacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size);
        Jacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size, const boost::container::vector<EquationPtr> &equations_ptrs);
        Jacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size, const boost::container::vector<std::shared_ptr<Equation>> &equations);
        Eigen::MatrixXd calculateJacobianMatrix(const Eigen::VectorXd &abscissa_value); // Jacobian matrix in a certain point.
        void setEquation(const EquationPtr equation_ptr);
        void setEquation(const std::shared_ptr<Equation> equation);
        void setEquations(const boost::container::vector<EquationPtr> &equations_ptrs);
        void setEquations(const boost::container::vector<std::shared_ptr<Equation>> &equations);
        void resetEquations();
        ~Jacobian();
};

class UnconstrainedJacobian : protected Jacobian {
    protected:
        boost::container::vector<KinematicEquationPtr> kinematic_equations_ptrs_;
        boost::container::vector<std::shared_ptr<KinematicEquation>> kinematic_equations_;

        const double partialDerivativeForwardKinematic(const KinematicEquationPtr equation_ptr, const unsigned int partial_variable_index, const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate);
        const double partialDerivativeForwardKinematic(std::shared_ptr<KinematicEquation> &equation, const unsigned int partial_variable_index, const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate);
        const double partialDerivativeInverseKinematic(const KinematicEquationPtr equation_ptr, const unsigned int partial_variable_index, const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate);
        const double partialDerivativeInverseKinematic(std::shared_ptr<KinematicEquation> &equation, const unsigned int partial_variable_index, const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate);
    public:
        UnconstrainedJacobian();
        UnconstrainedJacobian(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs);
        UnconstrainedJacobian(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations);
        UnconstrainedJacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size);
        UnconstrainedJacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size, const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs);
        UnconstrainedJacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size, const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations);
        Eigen::MatrixXd calculateForwardKinematicJacobianMatrix(const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate);
        Eigen::MatrixXd calculateInverseKinematicJacobianMatrix(const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate);
        void setKinematicEquation(const KinematicEquationPtr kinematic_equation_ptr);
        void setKinematicEquation(const std::shared_ptr<KinematicEquation> kinematic_equation);
        void setKinematicEquations(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs);
        void setKinematicEquations(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations);
        void resetEquations();
        ~UnconstrainedJacobian();
};

class ConstrainedJacobian : public UnconstrainedJacobian {
    protected:
        boost::container::vector<LagrangianAuxiliaryEquationPtr> lagrangian_auxiliary_equations_ptrs_;
        boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> lagrangian_auxiliary_equations_;

        const double partialDerivativeConstrainedForwardKinematic(const LagrangianAuxiliaryEquationPtr equation_ptr, const unsigned int partial_variable_index, const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate, const Eigen::VectorXd &multiplier);
        const double partialDerivativeConstrainedForwardKinematic(std::shared_ptr<LagrangianAuxiliaryEquation> &equation, const unsigned int partial_variable_index, const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate, const Eigen::VectorXd &multiplier);
        const double partialDerivativeConstrainedInverseKinematic(const LagrangianAuxiliaryEquationPtr equation_ptr, const unsigned int partial_variable_index, const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate, const Eigen::VectorXd &multiplier);
        const double partialDerivativeConstrainedInverseKinematic(std::shared_ptr<LagrangianAuxiliaryEquation> &equation, const unsigned int partial_variable_index, const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate, const Eigen::VectorXd &multiplier);
        const double partialDerivativeLagrangeMultiplier(const LagrangianAuxiliaryEquationPtr equation_ptr, const unsigned int partial_variable_index, const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &multiplier);
        const double partialDerivativeLagrangeMultiplier(std::shared_ptr<LagrangianAuxiliaryEquation> &equation, const unsigned int partial_variable_index, const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &multiplier);
    public:
        ConstrainedJacobian();
        ConstrainedJacobian(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs);
        ConstrainedJacobian(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations);
        ConstrainedJacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size);
        ConstrainedJacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size, const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs);
        ConstrainedJacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size, const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations);
        Eigen::MatrixXd calculateForwardKinematicJacobianMatrix(const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate, const Eigen::VectorXd &multiplier);
        Eigen::MatrixXd calculateInverseKinematicJacobianMatrix(const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate, const Eigen::VectorXd &multiplier);
        void setLagrangianAuxiliaryEquation(const LagrangianAuxiliaryEquationPtr lagrangian_auxiliary_equation_ptr);
        void setLagrangianAuxiliaryEquation(const std::shared_ptr<LagrangianAuxiliaryEquation> lagrangian_auxiliary_equation);
        void setLagrangianAuxiliaryEquations(const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs);
        void setLagrangianAuxiliaryEquations(const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations);
        void setKinematicAndLagrangianAuxiliaryEquations(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs);
        void setKinematicAndLagrangianAuxiliaryEquations(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations);
        void resetEquations();
        ~ConstrainedJacobian();
};

#endif /* __JACOBIAN_H__ */


#ifndef __SOLVER_H__
#define __SOLVER_H__

#include <memory>
#include <utility>
#include <boost/container/vector.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/math/tools/minima.hpp>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/SVD>
#include "afam_driver/solver/jacobian.h"

class Solver {
    protected:
        const unsigned int max_iteration_;
        const double error_tolerance_;
        const double epsilon_;

        Eigen::MatrixXd pseudoInverse(const Eigen::MatrixXd &matrix);
    public:
        Solver();
        Solver(unsigned int max_iteration, double error_tolerance, double epsilon);
        ~Solver();
};

class NewtonSolver : protected Solver {
    private:
        boost::container::vector<EquationPtr> equations_ptrs_;
        boost::container::vector<std::shared_ptr<Equation>> equations_;
    protected:
        virtual Eigen::VectorXd calculateSystemVector(const Eigen::VectorXd &value);
        virtual Eigen::VectorXd newtonMethod(const Eigen::VectorXd &initial_value);
    public:
        NewtonSolver();
        NewtonSolver(const boost::container::vector<EquationPtr> &equations_ptrs);
        NewtonSolver(const boost::container::vector<std::shared_ptr<Equation>> &equations);
        NewtonSolver(unsigned int max_iteration, double error_tolerance, double epsilon);
        NewtonSolver(unsigned int max_iteration, double error_tolerance, double epsilon, const boost::container::vector<EquationPtr> &equations_ptrs);
        NewtonSolver(unsigned int max_iteration, double error_tolerance, double epsilon, const boost::container::vector<std::shared_ptr<Equation>> &equations);
        void setEquation(const EquationPtr equation_ptr);
        void setEquation(const std::shared_ptr<Equation> equation);
        void setEquations(const boost::container::vector<EquationPtr> &equation_ptrs);
        void setEquations(const boost::container::vector<std::shared_ptr<Equation>> &equation);
        virtual Eigen::VectorXd solveEquation(const Eigen::VectorXd &initial_value);
        virtual void resetEquations();
        ~NewtonSolver();
};

class UnconstrainedNewtonSolver : protected NewtonSolver {
    protected:
        boost::container::vector<KinematicEquationPtr> kinematic_equations_ptrs_;
        boost::container::vector<std::shared_ptr<KinematicEquation>> kinematic_equations_;
        virtual Eigen::VectorXd calculateSystemVector(const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate);
        virtual Eigen::VectorXd newtonMethod(bool forward, const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate);
    public:
        UnconstrainedNewtonSolver();
        UnconstrainedNewtonSolver(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs);
        UnconstrainedNewtonSolver(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations);
        UnconstrainedNewtonSolver(unsigned int max_iteration, double error_tolerance, double epsilon);
        UnconstrainedNewtonSolver(unsigned int max_iteration, double error_tolerance, double epsilon, const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs);
        UnconstrainedNewtonSolver(unsigned int max_iteration, double error_tolerance, double epsilon, const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations);
        void setKinematicEquation(const KinematicEquationPtr kinematic_equation_ptr);
        void setKinematicEquation(const std::shared_ptr<KinematicEquation> kinematic_equation);
        void setKinematicEquations(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs);
        void setKinematicEquations(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations);
        virtual Eigen::VectorXd solveForwardKinematic(const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate);
        virtual Eigen::VectorXd solveInverseKinematic(const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate);
        virtual void resetEquations();
        ~UnconstrainedNewtonSolver();
};

class ConstrainedNewtonSolver : public UnconstrainedNewtonSolver {
    protected:
        boost::container::vector<LagrangianAuxiliaryEquationPtr> lagrangian_auxiliary_equations_ptrs_;
        boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> lagrangian_auxiliary_equations_;
        virtual Eigen::VectorXd calculateSystemVector(const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &multiplier);
        virtual Eigen::VectorXd newtonMethod(bool forward, const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &multiplier);
    public:
        ConstrainedNewtonSolver();
        ConstrainedNewtonSolver(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs);
        ConstrainedNewtonSolver(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations);
        ConstrainedNewtonSolver(unsigned int max_iteration, double error_tolerance, double epsilon);
        ConstrainedNewtonSolver(unsigned int max_iteration, double error_tolerance, double epsilon, const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs);
        ConstrainedNewtonSolver(unsigned int max_iteration, double error_tolerance, double epsilon, const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations);
        void setLagrangianAuxiliaryEquation(const LagrangianAuxiliaryEquationPtr lagrangian_auxiliary_equation_ptr);
        void setLagrangianAuxiliaryEquation(const std::shared_ptr<LagrangianAuxiliaryEquation> lagrangian_auxiliary_equation);
        void setLagrangianAuxiliaryEquations(const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs);
        void setLagrangianAuxiliaryEquations(const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations);
        void setKinematicAndLagrangianAuxiliaryEquations(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs);
        void setKinematicAndLagrangianAuxiliaryEquations(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations);
        virtual Eigen::VectorXd solveForwardKinematic(const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate, const Eigen::VectorXd &multiplier);
        virtual Eigen::VectorXd solveInverseKinematic(const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate, const Eigen::VectorXd &multiplier);
        virtual void resetEquations();
        ~ConstrainedNewtonSolver();
};

class ObjectivePhi : public ObjectiveFunction1D {
    private:
        Eigen::VectorXd current_pk_;
        Eigen::VectorXd current_sk_;
        ObjectiveFunctionPtr objective_function_ptr_;
        std::shared_ptr<ObjectiveFunction> objective_function_;
    public:
        ObjectivePhi() : ObjectiveFunction1D() {
            objective_function_ptr_ = nullptr;
        }
        ObjectivePhi(const ObjectiveFunctionPtr objective_function_ptr) : ObjectiveFunction1D() {
            objective_function_ptr_ = objective_function_ptr;
        }
        ObjectivePhi(std::shared_ptr<ObjectiveFunction> objective_function) : ObjectiveFunction1D() {
            objective_function_ = objective_function;
            objective_function_ptr_ = nullptr;
        }
        ObjectivePhi(const ObjectiveFunctionPtr objective_function_ptr, const Eigen::VectorXd &current_pk, const Eigen::VectorXd &current_sk) : ObjectiveFunction1D() {
            current_pk_ = current_pk;
            current_sk_ = current_sk;
            objective_function_ptr_ = objective_function_ptr;
        }
        ObjectivePhi(std::shared_ptr<ObjectiveFunction> &objective_function, const Eigen::VectorXd &current_pk, const Eigen::VectorXd &current_sk) : ObjectiveFunction1D() {
            current_pk_ = current_pk;
            current_sk_ = current_sk;
            objective_function_ = objective_function;
            objective_function_ptr_ = nullptr;
        }
        const double operator()(const double gamma) {
            if(objective_function_ptr_ != nullptr)
                return (*objective_function_ptr_)(current_pk_ + gamma*current_sk_);
            else
                return (*objective_function_)(current_pk_ + gamma*current_sk_);
        }
        void setEquation(const ObjectiveFunctionPtr objective_function_ptr) {
            objective_function_ptr_ = objective_function_ptr;
        }
        void setEquation(const std::shared_ptr<ObjectiveFunction> objective_function) {
            objective_function_ = objective_function;
        }
        void setObjectiveFunctionPkSk(const Eigen::VectorXd &current_pk, const Eigen::VectorXd &current_sk) {
            current_pk_ = current_pk;
            current_sk_ = current_sk;
        }
};

class GradientSolver : protected Solver {
    private:
        ObjectiveFunctionPtr objective_function_ptr_;
        std::shared_ptr<ObjectiveFunction> objective_function_;
    protected:
        double initial_step_size_;
        const double derivative(const std::shared_ptr<ObjectivePhi> &objective_phi, const double abscissa_value);
        void bracketInterval(const std::shared_ptr<ObjectivePhi> &objective_phi, double &lower_abscissa_value, double &middle_abscissa_value, double &upper_abscissa_value);
        const double partialDerivative(const ObjectiveFunctionPtr objective_function_ptr, const unsigned int partial_variable_index, const Eigen::VectorXd &abscissa_value);
        const double partialDerivative(const std::shared_ptr<ObjectiveFunction> &objective_function, const unsigned int partial_variable_index, const Eigen::VectorXd &abscissa_value);
        virtual Eigen::VectorXd calculateGradient(const Eigen::VectorXd &abscissa_value);
        double quadraticMinimization(const std::shared_ptr<ObjectivePhi> &objective_phi, double &lower_abscissa_value, double &upper_abscissa_value);
    public:
        GradientSolver();
        GradientSolver(const ObjectiveFunctionPtr objective_function_ptr);
        GradientSolver(const std::shared_ptr<ObjectiveFunction> objective_function);
        GradientSolver(unsigned int max_iteration, double error_tolerance, double epsilon, double initial_step_size);
        GradientSolver(unsigned int max_iteration, double error_tolerance, double epsilon, double initial_step_size, const ObjectiveFunctionPtr objective_function_ptr);
        GradientSolver(unsigned int max_iteration, double error_tolerance, double epsilon, double initial_step_size, const std::shared_ptr<ObjectiveFunction> objective_function);
        void setEquation(const ObjectiveFunctionPtr objective_function_ptr);
        void setEquation(const std::shared_ptr<ObjectiveFunction> &objective_function);
        virtual Eigen::VectorXd minimizeObjectiveFunction(const Eigen::VectorXd &initial_value);
        virtual void resetObjectiveFunction();
        ~GradientSolver();
};

#endif /* __SOLVER_H__ */

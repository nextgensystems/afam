#include "afam_driver/solver/jacobian.h"

Jacobian::Jacobian()
    : max_iteration_(MaxIteration::MAX_1000),
      error_tolerance_(Precision::MAX_DIGITS_12),
      initial_step_size_(InitialStepSize::SIZE_DIGITS_1) {}

Jacobian::Jacobian(const boost::container::vector<EquationPtr> &equations_ptrs)
    : max_iteration_(MaxIteration::MAX_1000),
      error_tolerance_(Precision::MAX_DIGITS_12),
      initial_step_size_(InitialStepSize::SIZE_DIGITS_1) {
    for(unsigned int i = 0; i < equations_ptrs.size(); i++){
        this->equations_ptrs_.push_back(equations_ptrs.at(i));
    }
}

Jacobian::Jacobian(const boost::container::vector<std::shared_ptr<Equation>> &equations)
    : max_iteration_(MaxIteration::MAX_1000),
      error_tolerance_(Precision::MAX_DIGITS_12),
      initial_step_size_(InitialStepSize::SIZE_DIGITS_1) {
    for(unsigned int i = 0; i < equations.size(); i++){
        this->equations_.push_back(equations.at(i));
    }
}

Jacobian::Jacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size)
    : max_iteration_(max_iteration),
      error_tolerance_(error_tolerance),
      initial_step_size_(initial_step_size) {}

Jacobian::Jacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size, const boost::container::vector<EquationPtr> &equations_ptrs)
    : max_iteration_(max_iteration),
      error_tolerance_(error_tolerance),
      initial_step_size_(initial_step_size) {
    for(unsigned int i = 0; i < equations_ptrs.size(); i++){
        this->equations_ptrs_.push_back(equations_ptrs.at(i));
    }
}

Jacobian::Jacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size, const boost::container::vector<std::shared_ptr<Equation>> &equations)
    : max_iteration_(max_iteration),
      error_tolerance_(error_tolerance),
      initial_step_size_(initial_step_size) {
    for(unsigned int i = 0; i < equations.size(); i++){
        this->equations_.push_back(equations.at(i));
    }
}

const double Jacobian::partialDerivative(const EquationPtr equation_ptr, const unsigned int partial_variable_index, const Eigen::VectorXd &abscissa_value) {
    boost::container::vector<double> step_sizes;
    boost::container::vector<double> approximate_derivatives;
    boost::container::vector<double> error_bounds;
    boost::container::vector<double> relative_errors;

    double step_size = initial_step_size_;
    Eigen::VectorXd increase_marginal_value = abscissa_value;
    Eigen::VectorXd decrease_marginal_value = abscissa_value;
    double marginal_value = abscissa_value[partial_variable_index];
    increase_marginal_value[partial_variable_index] = marginal_value + step_size;
    decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
    double error_bound;
    double approximate_value;
    step_sizes.push_back(step_size);
    approximate_derivatives.push_back(((*equation_ptr)(increase_marginal_value) - (*equation_ptr)(decrease_marginal_value)) / (2 * step_size));
    error_bounds.push_back(0);
    relative_errors.push_back(0);

    for(unsigned int i = 0; i < 2; i++){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation_ptr)(increase_marginal_value) - (*equation_ptr)(decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation_ptr)(increase_marginal_value) - (*equation_ptr)(decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+1]) + std::abs(approximate_derivatives[i])));
    }

    unsigned int i = 1;
    while(error_bounds[i] > error_bounds[i+1] && relative_errors[i] > error_tolerance_ && i < max_iteration_){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation_ptr)(increase_marginal_value) - (*equation_ptr)(decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation_ptr)(increase_marginal_value) - (*equation_ptr)(decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+2]) + std::abs(approximate_derivatives[i+1])));
        i++;
    }
    return approximate_derivatives[i];
}

const double Jacobian::partialDerivative(std::shared_ptr<Equation> &equation, const unsigned int partial_variable_index, const Eigen::VectorXd &abscissa_value) {
    boost::container::vector<double> step_sizes;
    boost::container::vector<double> approximate_derivatives;
    boost::container::vector<double> error_bounds;
    boost::container::vector<double> relative_errors;

    double step_size = initial_step_size_;
    Eigen::VectorXd increase_marginal_value = abscissa_value;
    Eigen::VectorXd decrease_marginal_value = abscissa_value;
    double marginal_value = abscissa_value[partial_variable_index];
    increase_marginal_value[partial_variable_index] = marginal_value + step_size;
    decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
    double error_bound;
    double approximate_value;
    step_sizes.push_back(step_size);
    approximate_derivatives.push_back(((*equation)(increase_marginal_value) - (*equation)(decrease_marginal_value)) / (2 * step_size));
    error_bounds.push_back(0);
    relative_errors.push_back(0);

    for(unsigned int i = 0; i < 2; i++){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation)(increase_marginal_value) - (*equation)(decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation)(increase_marginal_value) - (*equation)(decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+1]) + std::abs(approximate_derivatives[i])));
    }

    unsigned int i = 1;
    while(error_bounds[i] > error_bounds[i+1] && relative_errors[i] > error_tolerance_ && i < max_iteration_){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation)(increase_marginal_value) - (*equation)(decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation)(increase_marginal_value) - (*equation)(decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+2]) + std::abs(approximate_derivatives[i+1])));
        i++;
    }
    return approximate_derivatives[i];
}

Eigen::MatrixXd Jacobian::calculateJacobianMatrix(const Eigen::VectorXd &abscissa_value) {
    Eigen::MatrixXd jacobian_matrix(equations_ptrs_.size(), abscissa_value.size());
    if(!equations_ptrs_.empty()){
        for(unsigned int i = 0; i < equations_ptrs_.size(); i++){
            for(unsigned int j = 0; j < abscissa_value.size(); j++){
                jacobian_matrix.coeffRef(i, j) = partialDerivative(equations_ptrs_.at(i), j, abscissa_value);
            }
        }
    }else{
        jacobian_matrix.resize(equations_.size(), abscissa_value.size());
        for(unsigned int i = 0; i < equations_.size(); i++){
            for(unsigned int j = 0; j < abscissa_value.size(); j++){
                jacobian_matrix.coeffRef(i, j) = partialDerivative(equations_.at(i), j, abscissa_value);
            }
        }
    }

    return jacobian_matrix;
}

void Jacobian::setEquation(const EquationPtr equation_ptr) {
    equations_ptrs_.push_back(equation_ptr);
}

void Jacobian::setEquation(const std::shared_ptr<Equation> equation) {
    equations_.push_back(equation);
}

void Jacobian::setEquations(const boost::container::vector<EquationPtr> &equations_ptrs) {
    for(unsigned int i = 0; i < equations_ptrs.size(); i++){
        equations_ptrs_.push_back(equations_ptrs.at(i));
    }
}

void Jacobian::setEquations(const boost::container::vector<std::shared_ptr<Equation>> &equations) {
    for(unsigned int i = 0; i < equations.size(); i++){
        equations_.push_back(equations.at(i));
    }
}

void Jacobian::resetEquations() {
    equations_ptrs_.clear();
    equations_.clear();
}

Jacobian::~Jacobian() {}

UnconstrainedJacobian::UnconstrainedJacobian() : Jacobian() {}

UnconstrainedJacobian::UnconstrainedJacobian(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs) : Jacobian() {
    for(unsigned int i = 0; i < kinematic_equations_ptrs.size(); i++){
        this->kinematic_equations_ptrs_.push_back(kinematic_equations_ptrs.at(i));
    }
}

UnconstrainedJacobian::UnconstrainedJacobian(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations) : Jacobian() {
    for(unsigned int i = 0; i < kinematic_equations.size(); i++){
        this->kinematic_equations_.push_back(kinematic_equations.at(i));
    }
}

UnconstrainedJacobian::UnconstrainedJacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size)
    : Jacobian(max_iteration, error_tolerance, initial_step_size) {}

UnconstrainedJacobian::UnconstrainedJacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size, const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs)
    : Jacobian(max_iteration, error_tolerance, initial_step_size) {
    for(unsigned int i = 0; i < kinematic_equations_ptrs.size(); i++){
        this->kinematic_equations_ptrs_.push_back(kinematic_equations_ptrs.at(i));
    }
}

UnconstrainedJacobian::UnconstrainedJacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size, const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations)
    : Jacobian(max_iteration, error_tolerance, initial_step_size) {
    for(unsigned int i = 0; i < kinematic_equations.size(); i++){
        this->kinematic_equations_.push_back(kinematic_equations.at(i));
    }
}

const double UnconstrainedJacobian::partialDerivativeForwardKinematic(const KinematicEquationPtr equation_ptr, const unsigned int partial_variable_index, const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate) {
    boost::container::vector<double> step_sizes;
    boost::container::vector<double> approximate_derivatives;
    boost::container::vector<double> error_bounds;
    boost::container::vector<double> relative_errors;

    double step_size = initial_step_size_;
    Eigen::VectorXd increase_marginal_value = current_tip_coordinate;
    Eigen::VectorXd decrease_marginal_value = current_tip_coordinate;
    double marginal_value = current_tip_coordinate[partial_variable_index];
    increase_marginal_value[partial_variable_index] = marginal_value + step_size;
    decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
    double error_bound;
    double approximate_value;
    step_sizes.push_back(step_size);
    approximate_derivatives.push_back(((*equation_ptr)(planned_control_value, increase_marginal_value) - (*equation_ptr)(planned_control_value, decrease_marginal_value)) / (2 * step_size));
    error_bounds.push_back(0);
    relative_errors.push_back(0);

    for(unsigned int i = 0; i < 2; i++){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation_ptr)(planned_control_value, increase_marginal_value) - (*equation_ptr)(planned_control_value, decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation_ptr)(planned_control_value, increase_marginal_value) - (*equation_ptr)(planned_control_value, decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+1]) + std::abs(approximate_derivatives[i])));
    }

    unsigned int i = 1;
    while(error_bounds[i] > error_bounds[i+1] && relative_errors[i] > error_tolerance_ && i < max_iteration_){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation_ptr)(planned_control_value, increase_marginal_value) - (*equation_ptr)(planned_control_value, decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation_ptr)(planned_control_value, increase_marginal_value) - (*equation_ptr)(planned_control_value, decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+2]) + std::abs(approximate_derivatives[i+1])));
        i++;
    }
    return approximate_derivatives[i];
}

const double UnconstrainedJacobian::partialDerivativeForwardKinematic(std::shared_ptr<KinematicEquation> &equation, const unsigned int partial_variable_index, const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate) {
    boost::container::vector<double> step_sizes;
    boost::container::vector<double> approximate_derivatives;
    boost::container::vector<double> error_bounds;
    boost::container::vector<double> relative_errors;

    double step_size = initial_step_size_;
    Eigen::VectorXd increase_marginal_value = current_tip_coordinate;
    Eigen::VectorXd decrease_marginal_value = current_tip_coordinate;
    double marginal_value = current_tip_coordinate[partial_variable_index];
    increase_marginal_value[partial_variable_index] = marginal_value + step_size;
    decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
    double error_bound;
    double approximate_value;
    step_sizes.push_back(step_size);
    approximate_derivatives.push_back(((*equation)(planned_control_value, increase_marginal_value) - (*equation)(planned_control_value, decrease_marginal_value)) / (2 * step_size));
    error_bounds.push_back(0);
    relative_errors.push_back(0);

    for(unsigned int i = 0; i < 2; i++){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation)(planned_control_value, increase_marginal_value) - (*equation)(planned_control_value, decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation)(planned_control_value, increase_marginal_value) - (*equation)(planned_control_value, decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+1]) + std::abs(approximate_derivatives[i])));
    }

    unsigned int i = 1;
    while(error_bounds[i] > error_bounds[i+1] && relative_errors[i] > error_tolerance_ && i < max_iteration_){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation)(planned_control_value, increase_marginal_value) - (*equation)(planned_control_value, decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation)(planned_control_value, increase_marginal_value) - (*equation)(planned_control_value, decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+2]) + std::abs(approximate_derivatives[i+1])));
        i++;
    }
    return approximate_derivatives[i];
}

const double UnconstrainedJacobian::partialDerivativeInverseKinematic(const KinematicEquationPtr equation_ptr, const unsigned int partial_variable_index, const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate) {
    boost::container::vector<double> step_sizes;
    boost::container::vector<double> approximate_derivatives;
    boost::container::vector<double> error_bounds;
    boost::container::vector<double> relative_errors;

    double step_size = initial_step_size_;
    Eigen::VectorXd increase_marginal_value = current_control_value;
    Eigen::VectorXd decrease_marginal_value = current_control_value;
    double marginal_value = current_control_value[partial_variable_index];
    increase_marginal_value[partial_variable_index] = marginal_value + step_size;
    decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
    double error_bound;
    double approximate_value;
    step_sizes.push_back(step_size);
    approximate_derivatives.push_back(((*equation_ptr)(increase_marginal_value, planned_tip_coordinate) - (*equation_ptr)(decrease_marginal_value, planned_tip_coordinate)) / (2 * step_size));
    error_bounds.push_back(0);
    relative_errors.push_back(0);
    for(unsigned int i = 0; i < 2; i++){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation_ptr)(increase_marginal_value, planned_tip_coordinate) - (*equation_ptr)(decrease_marginal_value, planned_tip_coordinate)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation_ptr)(increase_marginal_value, planned_tip_coordinate) - (*equation_ptr)(decrease_marginal_value, planned_tip_coordinate)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+1]) + std::abs(approximate_derivatives[i])));
    }

    unsigned int i = 1;
    while(error_bounds[i] > error_bounds[i+1] && relative_errors[i] > error_tolerance_ && i < max_iteration_){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation_ptr)(increase_marginal_value, planned_tip_coordinate) - (*equation_ptr)(decrease_marginal_value, planned_tip_coordinate)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation_ptr)(increase_marginal_value, planned_tip_coordinate) - (*equation_ptr)(decrease_marginal_value, planned_tip_coordinate)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+2]) + std::abs(approximate_derivatives[i+1])));
        i++;
    }
    return approximate_derivatives[i];
}

const double UnconstrainedJacobian::partialDerivativeInverseKinematic(std::shared_ptr<KinematicEquation> &equation, const unsigned int partial_variable_index, const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate) {
    boost::container::vector<double> step_sizes;
    boost::container::vector<double> approximate_derivatives;
    boost::container::vector<double> error_bounds;
    boost::container::vector<double> relative_errors;

    double step_size = initial_step_size_;
    Eigen::VectorXd increase_marginal_value = current_control_value;
    Eigen::VectorXd decrease_marginal_value = current_control_value;
    double marginal_value = current_control_value[partial_variable_index];
    increase_marginal_value[partial_variable_index] = marginal_value + step_size;
    decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
    double error_bound;
    double approximate_value;
    step_sizes.push_back(step_size);
    approximate_value = ((*equation)(increase_marginal_value, planned_tip_coordinate) - (*equation)(decrease_marginal_value, planned_tip_coordinate)) / (2 * step_size);
    approximate_derivatives.push_back(((*equation)(increase_marginal_value, planned_tip_coordinate) - (*equation)(decrease_marginal_value, planned_tip_coordinate)) / (2 * step_size));
    error_bounds.push_back(0);
    relative_errors.push_back(0);
    for(unsigned int i = 0; i < 2; i++){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_derivatives.push_back(((*equation)(increase_marginal_value, planned_tip_coordinate) - (*equation)(decrease_marginal_value, planned_tip_coordinate)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+1]) + std::abs(approximate_derivatives[i])));
    }

    unsigned int i = 1;
    while(error_bounds[i] > error_bounds[i+1] && relative_errors[i] > error_tolerance_ && i < max_iteration_){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_derivatives.push_back(((*equation)(increase_marginal_value, planned_tip_coordinate) - (*equation)(decrease_marginal_value, planned_tip_coordinate)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+2]) + std::abs(approximate_derivatives[i+1])));
        i++;
    }
    return approximate_derivatives[i];
}

Eigen::MatrixXd UnconstrainedJacobian::calculateForwardKinematicJacobianMatrix(const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate) {
    Eigen::MatrixXd jacobian_matrix(kinematic_equations_ptrs_.size(), current_tip_coordinate.size());
    if(!kinematic_equations_ptrs_.empty()){
        for(unsigned int i = 0; i < kinematic_equations_ptrs_.size(); i++){
            for(unsigned int j = 0; j < current_tip_coordinate.size(); j++){
                jacobian_matrix.coeffRef(i, j) = partialDerivativeForwardKinematic(kinematic_equations_ptrs_.at(i), j, planned_control_value, current_tip_coordinate);
            }
        }
    }else{
        jacobian_matrix.resize(kinematic_equations_.size(), current_tip_coordinate.size());
        for(unsigned int i = 0; i < kinematic_equations_.size(); i++){
            for(unsigned int j = 0; j < current_tip_coordinate.size(); j++){
                jacobian_matrix.coeffRef(i, j) = partialDerivativeForwardKinematic(kinematic_equations_.at(i), j, planned_control_value, current_tip_coordinate);
            }
        }
    }
    return jacobian_matrix;
}

Eigen::MatrixXd UnconstrainedJacobian::calculateInverseKinematicJacobianMatrix(const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate) {
    Eigen::MatrixXd jacobian_matrix(kinematic_equations_ptrs_.size(), current_control_value.size());
    if(!kinematic_equations_ptrs_.empty()){
        for(unsigned int i = 0; i < kinematic_equations_ptrs_.size(); i++){
            for(unsigned int j = 0; j < current_control_value.size(); j++){
                jacobian_matrix.coeffRef(i, j) = partialDerivativeInverseKinematic(kinematic_equations_ptrs_.at(i), j, current_control_value, planned_tip_coordinate);
            }
        }
    }else{
        jacobian_matrix.resize(kinematic_equations_.size(), current_control_value.size());
        for(unsigned int i = 0; i < kinematic_equations_.size(); i++){
            for(unsigned int j = 0; j < current_control_value.size(); j++){
                jacobian_matrix.coeffRef(i, j) = partialDerivativeInverseKinematic(kinematic_equations_.at(i), j, current_control_value, planned_tip_coordinate);
            }
        }
    }
    return jacobian_matrix;
}

void UnconstrainedJacobian::setKinematicEquation(const KinematicEquationPtr kinematic_equation_ptr) {
    kinematic_equations_ptrs_.push_back(kinematic_equation_ptr);
}

void UnconstrainedJacobian::setKinematicEquation(const std::shared_ptr<KinematicEquation> kinematic_equation) {
    kinematic_equations_.push_back(kinematic_equation);
}

void UnconstrainedJacobian::setKinematicEquations(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs) {
    for(unsigned int i = 0; i < kinematic_equations_ptrs.size(); i++){
        kinematic_equations_ptrs_.push_back(kinematic_equations_ptrs.at(i));
    }
}

void UnconstrainedJacobian::setKinematicEquations(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations) {
    for(unsigned int i = 0; i < kinematic_equations.size(); i++){
        kinematic_equations_.push_back(kinematic_equations.at(i));
    }
}

void UnconstrainedJacobian::resetEquations() {
    kinematic_equations_ptrs_.clear();
    kinematic_equations_.clear();
}

UnconstrainedJacobian::~UnconstrainedJacobian() {}

ConstrainedJacobian::ConstrainedJacobian() : UnconstrainedJacobian() {}

ConstrainedJacobian::ConstrainedJacobian(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs)
    : UnconstrainedJacobian(kinematic_equations_ptrs) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs.size(); i++){
        this->lagrangian_auxiliary_equations_ptrs_.push_back(lagrangian_auxiliary_equations_ptrs.at(i));
    }
}

ConstrainedJacobian::ConstrainedJacobian(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations)
    : UnconstrainedJacobian(kinematic_equations) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations.size(); i++){
        this->lagrangian_auxiliary_equations_.push_back(lagrangian_auxiliary_equations.at(i));
    }
}

ConstrainedJacobian::ConstrainedJacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size)
    : UnconstrainedJacobian(max_iteration, error_tolerance, initial_step_size) {}

ConstrainedJacobian::ConstrainedJacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size, const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs)
    : UnconstrainedJacobian(max_iteration, error_tolerance, initial_step_size, kinematic_equations_ptrs) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs.size(); i++){
        this->lagrangian_auxiliary_equations_ptrs_.push_back(lagrangian_auxiliary_equations_ptrs.at(i));
    }
}

ConstrainedJacobian::ConstrainedJacobian(unsigned int max_iteration, double error_tolerance, double initial_step_size, const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations)
    : UnconstrainedJacobian(max_iteration, error_tolerance, initial_step_size, kinematic_equations) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations.size(); i++){
        this->lagrangian_auxiliary_equations_.push_back(lagrangian_auxiliary_equations.at(i));
    }
}

const double ConstrainedJacobian::partialDerivativeConstrainedForwardKinematic(const LagrangianAuxiliaryEquationPtr equation_ptr, const unsigned int partial_variable_index, const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate, const Eigen::VectorXd &multiplier) {
    boost::container::vector<double> step_sizes;
    boost::container::vector<double> approximate_derivatives;
    boost::container::vector<double> error_bounds;
    boost::container::vector<double> relative_errors;

    double step_size = initial_step_size_;
    Eigen::VectorXd increase_marginal_value = current_tip_coordinate;
    Eigen::VectorXd decrease_marginal_value = current_tip_coordinate;
    double marginal_value = current_tip_coordinate[partial_variable_index];
    increase_marginal_value[partial_variable_index] = marginal_value + step_size;
    decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
    double error_bound;
    double approximate_value;
    step_sizes.push_back(step_size);
    approximate_derivatives.push_back(((*equation_ptr)(planned_control_value, increase_marginal_value, multiplier) - (*equation_ptr)(planned_control_value, decrease_marginal_value, multiplier)) / (2 * step_size));
    error_bounds.push_back(0);
    relative_errors.push_back(0);

    for(unsigned int i = 0; i < 2; i++){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation_ptr)(planned_control_value, increase_marginal_value, multiplier) - (*equation_ptr)(planned_control_value, decrease_marginal_value, multiplier)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation_ptr)(planned_control_value, increase_marginal_value, multiplier) - (*equation_ptr)(planned_control_value, decrease_marginal_value, multiplier)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+1]) + std::abs(approximate_derivatives[i])));
    }

    unsigned int i = 1;
    while(error_bounds[i] > error_bounds[i+1] && relative_errors[i] > error_tolerance_ && i < max_iteration_){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation_ptr)(planned_control_value, increase_marginal_value, multiplier) - (*equation_ptr)(planned_control_value, decrease_marginal_value, multiplier)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation_ptr)(planned_control_value, increase_marginal_value, multiplier) - (*equation_ptr)(planned_control_value, decrease_marginal_value, multiplier)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+2]) + std::abs(approximate_derivatives[i+1])));
        i++;
    }
    return approximate_derivatives[i];
}

const double ConstrainedJacobian::partialDerivativeConstrainedForwardKinematic(std::shared_ptr<LagrangianAuxiliaryEquation> &equation, const unsigned int partial_variable_index, const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate, const Eigen::VectorXd &multiplier) {
    boost::container::vector<double> step_sizes;
    boost::container::vector<double> approximate_derivatives;
    boost::container::vector<double> error_bounds;
    boost::container::vector<double> relative_errors;

    double step_size = initial_step_size_;
    Eigen::VectorXd increase_marginal_value = current_tip_coordinate;
    Eigen::VectorXd decrease_marginal_value = current_tip_coordinate;
    double marginal_value = current_tip_coordinate[partial_variable_index];
    increase_marginal_value[partial_variable_index] = marginal_value + step_size;
    decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
    double error_bound;
    double approximate_value;
    step_sizes.push_back(step_size);
    approximate_derivatives.push_back(((*equation)(planned_control_value, increase_marginal_value, multiplier) - (*equation)(planned_control_value, decrease_marginal_value, multiplier)) / (2 * step_size));
    error_bounds.push_back(0);
    relative_errors.push_back(0);

    for(unsigned int i = 0; i < 2; i++){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation)(planned_control_value, increase_marginal_value, multiplier) - (*equation)(planned_control_value, decrease_marginal_value, multiplier)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation)(planned_control_value, increase_marginal_value, multiplier) - (*equation)(planned_control_value, decrease_marginal_value, multiplier)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+1]) + std::abs(approximate_derivatives[i])));
    }

    unsigned int i = 1;
    while(error_bounds[i] > error_bounds[i+1] && relative_errors[i] > error_tolerance_ && i < max_iteration_){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation)(planned_control_value, increase_marginal_value, multiplier) - (*equation)(planned_control_value, decrease_marginal_value, multiplier)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation)(planned_control_value, increase_marginal_value, multiplier) - (*equation)(planned_control_value, decrease_marginal_value, multiplier)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+2]) + std::abs(approximate_derivatives[i+1])));
        i++;
    }
    return approximate_derivatives[i];
}

const double ConstrainedJacobian::partialDerivativeConstrainedInverseKinematic(const LagrangianAuxiliaryEquationPtr equation_ptr, const unsigned int partial_variable_index, const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate, const Eigen::VectorXd &multiplier) {
    boost::container::vector<double> step_sizes;
    boost::container::vector<double> approximate_derivatives;
    boost::container::vector<double> error_bounds;
    boost::container::vector<double> relative_errors;

    double step_size = initial_step_size_;
    Eigen::VectorXd increase_marginal_value = current_control_value;
    Eigen::VectorXd decrease_marginal_value = current_control_value;
    double marginal_value = current_control_value[partial_variable_index];
    increase_marginal_value[partial_variable_index] = marginal_value + step_size;
    decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
    double error_bound;
    double approximate_value;
    step_sizes.push_back(step_size);
    approximate_derivatives.push_back(((*equation_ptr)(increase_marginal_value, planned_tip_coordinate, multiplier) - (*equation_ptr)(decrease_marginal_value, planned_tip_coordinate, multiplier)) / (2 * step_size));
    error_bounds.push_back(0);
    relative_errors.push_back(0);

    for(unsigned int i = 0; i < 2; i++){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation_ptr)(increase_marginal_value, planned_tip_coordinate, multiplier) - (*equation_ptr)(decrease_marginal_value, planned_tip_coordinate, multiplier)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation_ptr)(increase_marginal_value, planned_tip_coordinate, multiplier) - (*equation_ptr)(decrease_marginal_value, planned_tip_coordinate, multiplier)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+1]) + std::abs(approximate_derivatives[i])));
    }

    unsigned int i = 1;
    while(error_bounds[i] > error_bounds[i+1] && relative_errors[i] > error_tolerance_ && i < max_iteration_){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation_ptr)(increase_marginal_value, planned_tip_coordinate, multiplier) - (*equation_ptr)(decrease_marginal_value, planned_tip_coordinate, multiplier)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation_ptr)(increase_marginal_value, planned_tip_coordinate, multiplier) - (*equation_ptr)(decrease_marginal_value, planned_tip_coordinate, multiplier)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+2]) + std::abs(approximate_derivatives[i+1])));
        i++;
    }
    return approximate_derivatives[i];
}

const double ConstrainedJacobian::partialDerivativeConstrainedInverseKinematic(std::shared_ptr<LagrangianAuxiliaryEquation> &equation, const unsigned int partial_variable_index, const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate, const Eigen::VectorXd &multiplier) {
    boost::container::vector<double> step_sizes;
    boost::container::vector<double> approximate_derivatives;
    boost::container::vector<double> error_bounds;
    boost::container::vector<double> relative_errors;

    double step_size = initial_step_size_;
    Eigen::VectorXd increase_marginal_value = current_control_value;
    Eigen::VectorXd decrease_marginal_value = current_control_value;
    double marginal_value = current_control_value[partial_variable_index];
    increase_marginal_value[partial_variable_index] = marginal_value + step_size;
    decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
    double error_bound;
    double approximate_value;
    step_sizes.push_back(step_size);
    approximate_derivatives.push_back(((*equation)(increase_marginal_value, planned_tip_coordinate, multiplier) - (*equation)(decrease_marginal_value, planned_tip_coordinate, multiplier)) / (2 * step_size));
    error_bounds.push_back(0);
    relative_errors.push_back(0);

    for(unsigned int i = 0; i < 2; i++){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation)(increase_marginal_value, planned_tip_coordinate, multiplier) - (*equation)(decrease_marginal_value, planned_tip_coordinate, multiplier)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation)(increase_marginal_value, planned_tip_coordinate, multiplier) - (*equation)(decrease_marginal_value, planned_tip_coordinate, multiplier)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+1]) + std::abs(approximate_derivatives[i])));
    }

    unsigned int i = 1;
    while(error_bounds[i] > error_bounds[i+1] && relative_errors[i] > error_tolerance_ && i < max_iteration_){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation)(increase_marginal_value, planned_tip_coordinate, multiplier) - (*equation)(decrease_marginal_value, planned_tip_coordinate, multiplier)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation)(increase_marginal_value, planned_tip_coordinate, multiplier) - (*equation)(decrease_marginal_value, planned_tip_coordinate, multiplier)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+2]) + std::abs(approximate_derivatives[i+1])));
        i++;
    }
    return approximate_derivatives[i];
}

const double ConstrainedJacobian::partialDerivativeLagrangeMultiplier(const LagrangianAuxiliaryEquationPtr equation_ptr, const unsigned int partial_variable_index, const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &multiplier) {
    boost::container::vector<double> step_sizes;
    boost::container::vector<double> approximate_derivatives;
    boost::container::vector<double> error_bounds;
    boost::container::vector<double> relative_errors;

    double step_size = initial_step_size_;
    Eigen::VectorXd increase_marginal_value = multiplier;
    Eigen::VectorXd decrease_marginal_value = multiplier;
    double marginal_value = multiplier[partial_variable_index];
    increase_marginal_value[partial_variable_index] = marginal_value + step_size;
    decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
    double error_bound;
    double approximate_value;
    step_sizes.push_back(step_size);
    approximate_derivatives.push_back(((*equation_ptr)(control_value, tip_coordinate, increase_marginal_value) - (*equation_ptr)(control_value, tip_coordinate, decrease_marginal_value)) / (2 * step_size));
    error_bounds.push_back(0);
    relative_errors.push_back(0);

    for(unsigned int i = 0; i < 2; i++){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation_ptr)(control_value, tip_coordinate, increase_marginal_value) - (*equation_ptr)(control_value, tip_coordinate, decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation_ptr)(control_value, tip_coordinate, increase_marginal_value) - (*equation_ptr)(control_value, tip_coordinate, decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+1]) + std::abs(approximate_derivatives[i])));
    }

    unsigned int i = 1;
    while(error_bounds[i] > error_bounds[i+1] && relative_errors[i] > error_tolerance_ && i < max_iteration_){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation_ptr)(control_value, tip_coordinate, increase_marginal_value) - (*equation_ptr)(control_value, tip_coordinate, decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation_ptr)(control_value, tip_coordinate, increase_marginal_value) - (*equation_ptr)(control_value, tip_coordinate, decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+2]) + std::abs(approximate_derivatives[i+1])));
        i++;
    }
    return approximate_derivatives[i];
}

const double ConstrainedJacobian::partialDerivativeLagrangeMultiplier(std::shared_ptr<LagrangianAuxiliaryEquation> &equation, const unsigned int partial_variable_index, const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &multiplier) {
    boost::container::vector<double> step_sizes;
    boost::container::vector<double> approximate_derivatives;
    boost::container::vector<double> error_bounds;
    boost::container::vector<double> relative_errors;

    double step_size = initial_step_size_;
    Eigen::VectorXd increase_marginal_value = multiplier;
    Eigen::VectorXd decrease_marginal_value = multiplier;
    double marginal_value = multiplier[partial_variable_index];
    increase_marginal_value[partial_variable_index] = marginal_value + step_size;
    decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
    double error_bound;
    double approximate_value;
    step_sizes.push_back(step_size);
    approximate_derivatives.push_back(((*equation)(control_value, tip_coordinate, increase_marginal_value) - (*equation)(control_value, tip_coordinate, decrease_marginal_value)) / (2 * step_size));
    error_bounds.push_back(0);
    relative_errors.push_back(0);

    for(unsigned int i = 0; i < 2; i++){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation)(control_value, tip_coordinate, increase_marginal_value) - (*equation)(control_value, tip_coordinate, decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation)(control_value, tip_coordinate, increase_marginal_value) - (*equation)(control_value, tip_coordinate, decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+1]) + std::abs(approximate_derivatives[i])));
    }

    unsigned int i = 1;
    while(error_bounds[i] > error_bounds[i+1] && relative_errors[i] > error_tolerance_ && i < max_iteration_){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*equation)(control_value, tip_coordinate, increase_marginal_value) - (*equation)(control_value, tip_coordinate, decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*equation)(control_value, tip_coordinate, increase_marginal_value) - (*equation)(control_value, tip_coordinate, decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+2]) + std::abs(approximate_derivatives[i+1])));
        i++;
    }
    return approximate_derivatives[i];
}

Eigen::MatrixXd ConstrainedJacobian::calculateForwardKinematicJacobianMatrix(const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate, const Eigen::VectorXd &multiplier) {
    Eigen::MatrixXd jacobian_matrix(kinematic_equations_ptrs_.size() + lagrangian_auxiliary_equations_ptrs_.size(), current_tip_coordinate.size() + multiplier.size());
    if(!kinematic_equations_ptrs_.empty()){
        for(unsigned int i = 0; i < kinematic_equations_ptrs_.size(); i++){
            for(unsigned int j = 0; j < current_tip_coordinate.size(); j++){
                jacobian_matrix.coeffRef(i, j) = partialDerivativeForwardKinematic(kinematic_equations_ptrs_.at(i), j, planned_control_value, current_tip_coordinate);
            }
            for(unsigned int j = 0; j < multiplier.size(); j++){
                jacobian_matrix.coeffRef(i, current_tip_coordinate.size()+j) = 0;
            }
        }

        for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs_.size(); i++){
            for(unsigned int j = 0; j < current_tip_coordinate.size(); j++){
                jacobian_matrix.coeffRef(kinematic_equations_ptrs_.size()+i, j) = partialDerivativeConstrainedForwardKinematic(lagrangian_auxiliary_equations_ptrs_.at(i), j, planned_control_value, current_tip_coordinate, multiplier);
            }
            for(unsigned int j = 0; j < multiplier.size(); j++){
                jacobian_matrix.coeffRef(kinematic_equations_ptrs_.size()+i, current_tip_coordinate.size()+j) = partialDerivativeLagrangeMultiplier(lagrangian_auxiliary_equations_ptrs_.at(i), j, planned_control_value, current_tip_coordinate, multiplier);
            }
        }
    }else{
        jacobian_matrix.resize(kinematic_equations_.size() + lagrangian_auxiliary_equations_.size(), current_tip_coordinate.size() + multiplier.size());
        for(unsigned int i = 0; i < kinematic_equations_.size(); i++){
            for(unsigned int j = 0; j < current_tip_coordinate.size(); j++){
                jacobian_matrix.coeffRef(i, j) = partialDerivativeForwardKinematic(kinematic_equations_.at(i), j, planned_control_value, current_tip_coordinate);
            }
            for(unsigned int j = 0; j < multiplier.size(); j++){
                jacobian_matrix.coeffRef(i, current_tip_coordinate.size()+j) = 0;
            }
        }

        for(unsigned int i = 0; i < lagrangian_auxiliary_equations_.size(); i++){
            for(unsigned int j = 0; j < current_tip_coordinate.size(); j++){
                jacobian_matrix.coeffRef(kinematic_equations_.size()+i, j) = partialDerivativeConstrainedForwardKinematic(lagrangian_auxiliary_equations_.at(i), j, planned_control_value, current_tip_coordinate, multiplier);
            }
            for(unsigned int j = 0; j < multiplier.size(); j++){
                jacobian_matrix.coeffRef(kinematic_equations_.size()+i, current_tip_coordinate.size()+j) = partialDerivativeLagrangeMultiplier(lagrangian_auxiliary_equations_.at(i), j, planned_control_value, current_tip_coordinate, multiplier);
            }
        }
    }
    return jacobian_matrix;
}

Eigen::MatrixXd ConstrainedJacobian::calculateInverseKinematicJacobianMatrix(const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate, const Eigen::VectorXd &multiplier) {
    Eigen::MatrixXd jacobian_matrix(kinematic_equations_ptrs_.size() + lagrangian_auxiliary_equations_ptrs_.size(), current_control_value.size() + multiplier.size());
    if(!kinematic_equations_ptrs_.empty()){
        for(unsigned int i = 0; i < kinematic_equations_ptrs_.size(); i++){
            for(unsigned int j = 0; j < current_control_value.size(); j++){
                jacobian_matrix.coeffRef(i, j) = partialDerivativeInverseKinematic(kinematic_equations_ptrs_.at(i), j, current_control_value, planned_tip_coordinate);
            }
            for(unsigned int j = 0; j < multiplier.size(); j++){
                jacobian_matrix.coeffRef(i, current_control_value.size()+j) = 0;
            }
        }

        for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs_.size(); i++){
            for(unsigned int j = 0; j < current_control_value.size(); j++){
                jacobian_matrix.coeffRef(kinematic_equations_ptrs_.size()+i, j) = partialDerivativeConstrainedInverseKinematic(lagrangian_auxiliary_equations_ptrs_.at(i), j, current_control_value, planned_tip_coordinate, multiplier);
            }
            for(unsigned int j = 0; j < multiplier.size(); j++){
                jacobian_matrix.coeffRef(kinematic_equations_ptrs_.size()+i, current_control_value.size()+j) = partialDerivativeLagrangeMultiplier(lagrangian_auxiliary_equations_ptrs_.at(i), j, current_control_value, planned_tip_coordinate, multiplier);
            }
        }
    }else{
        jacobian_matrix.resize(kinematic_equations_.size() + lagrangian_auxiliary_equations_.size(), current_control_value.size() + multiplier.size());
        for(unsigned int i = 0; i < kinematic_equations_.size(); i++){
            for(unsigned int j = 0; j < current_control_value.size(); j++){
                jacobian_matrix.coeffRef(i, j) = partialDerivativeInverseKinematic(kinematic_equations_.at(i), j, current_control_value, planned_tip_coordinate);
            }
            for(unsigned int j = 0; j < multiplier.size(); j++){
                jacobian_matrix.coeffRef(i, current_control_value.size()+j) = 0;
            }
        }

        for(unsigned int i = 0; i < lagrangian_auxiliary_equations_.size(); i++){
            for(unsigned int j = 0; j < current_control_value.size(); j++){
                jacobian_matrix.coeffRef(kinematic_equations_.size()+i, j) = partialDerivativeConstrainedInverseKinematic(lagrangian_auxiliary_equations_.at(i), j, current_control_value, planned_tip_coordinate, multiplier);
            }
            for(unsigned int j = 0; j < multiplier.size(); j++){
                jacobian_matrix.coeffRef(kinematic_equations_.size()+i, current_control_value.size()+j) = partialDerivativeLagrangeMultiplier(lagrangian_auxiliary_equations_.at(i), j, current_control_value, planned_tip_coordinate, multiplier);
            }
        }
    }

    return jacobian_matrix;
}


void ConstrainedJacobian::setLagrangianAuxiliaryEquation(const LagrangianAuxiliaryEquationPtr lagrangian_auxiliary_equation_ptr) {
    lagrangian_auxiliary_equations_ptrs_.push_back(lagrangian_auxiliary_equation_ptr);
}

void ConstrainedJacobian::setLagrangianAuxiliaryEquation(const std::shared_ptr<LagrangianAuxiliaryEquation> lagrangian_auxiliary_equation) {
    lagrangian_auxiliary_equations_.push_back(lagrangian_auxiliary_equation);
}

void ConstrainedJacobian::setLagrangianAuxiliaryEquations(const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs.size(); i++){
        lagrangian_auxiliary_equations_ptrs_.push_back(lagrangian_auxiliary_equations_ptrs.at(i));
    }
}

void ConstrainedJacobian::setLagrangianAuxiliaryEquations(const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations.size(); i++){
        lagrangian_auxiliary_equations_.push_back(lagrangian_auxiliary_equations.at(i));
    }
}

void ConstrainedJacobian::setKinematicAndLagrangianAuxiliaryEquations(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs) {
    for(unsigned int i = 0; i < kinematic_equations_ptrs.size(); i++){
        kinematic_equations_ptrs_.push_back(kinematic_equations_ptrs.at(i));
    }
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs.size(); i++){
        lagrangian_auxiliary_equations_ptrs_.push_back(lagrangian_auxiliary_equations_ptrs.at(i));
    }
}

void ConstrainedJacobian::setKinematicAndLagrangianAuxiliaryEquations(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations) {
    for(unsigned int i = 0; i < kinematic_equations.size(); i++){
        kinematic_equations_.push_back(kinematic_equations.at(i));
    }
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations.size(); i++){
        lagrangian_auxiliary_equations_.push_back(lagrangian_auxiliary_equations.at(i));
    }
}

void ConstrainedJacobian::resetEquations() {
    UnconstrainedJacobian::resetEquations();
    lagrangian_auxiliary_equations_ptrs_.clear();
    lagrangian_auxiliary_equations_.clear();
}

ConstrainedJacobian::~ConstrainedJacobian() {}

#include "afam_driver/solver/solver.h"

Solver::Solver()
    : max_iteration_(MaxIteration::MAX_1000),
      error_tolerance_(Precision::MAX_DIGITS_12),
      epsilon_(Precision::MAX_DIGITS_12) {}

Solver::Solver(unsigned int max_iteration, double error_tolerance, double epsilon)
    : max_iteration_(max_iteration),
      error_tolerance_(error_tolerance),
      epsilon_(epsilon) {}

Eigen::MatrixXd Solver::pseudoInverse(const Eigen::MatrixXd &matrix) {
    Eigen::MatrixXd inverse_matrix(matrix.rows(), matrix.cols());
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(matrix, Eigen::ComputeThinU | Eigen::ComputeThinV);
    double epsilon = std::numeric_limits<double>::epsilon();
    double tolerance = epsilon * std::max(matrix.cols(), matrix.rows()) *svd.singularValues().array().abs()(0);
    inverse_matrix = svd.matrixV() * (svd.singularValues().array().abs() > tolerance).select(svd.singularValues().array().inverse(), 0).matrix().asDiagonal() * svd.matrixU().adjoint();
    return inverse_matrix;
}

Solver::~Solver() {}

NewtonSolver::NewtonSolver()
    : Solver() {}

NewtonSolver::NewtonSolver(const boost::container::vector<EquationPtr> &equations_ptrs)
    : Solver() {
    for(unsigned int i = 0; i < equations_ptrs.size(); i++){
        equations_ptrs_.push_back(equations_ptrs.at(i));
    }
}

NewtonSolver::NewtonSolver(const boost::container::vector<std::shared_ptr<Equation>> &equations)
    : Solver() {
    for(unsigned int i = 0; i < equations.size(); i++){
        equations_.push_back(equations.at(i));
    }
}

NewtonSolver::NewtonSolver(unsigned int max_iteration, double error_tolerance, double epsilon)
    : Solver(max_iteration, error_tolerance, epsilon) {}

NewtonSolver::NewtonSolver(unsigned int max_iteration, double error_tolerance, double epsilon, const boost::container::vector<EquationPtr> &equations_ptrs)
    : Solver(max_iteration, error_tolerance, epsilon) {
    for(unsigned int i = 0; i < equations_ptrs.size(); i++){
        equations_ptrs_.push_back(equations_ptrs.at(i));
    }
}

NewtonSolver::NewtonSolver(unsigned int max_iteration, double error_tolerance, double epsilon, const boost::container::vector<std::shared_ptr<Equation>> &equations)
    : Solver(max_iteration, error_tolerance, epsilon) {
    for(unsigned int i = 0; i < equations.size(); i++){
        equations_.push_back(equations.at(i));
    }
}

/*Eigen::MatrixXd NewtonSolver::pseudoInverse(const Eigen::MatrixXd &matrix) {
    Eigen::MatrixXd inverse_matrix(matrix.rows(), matrix.cols());
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(matrix, Eigen::ComputeThinU | Eigen::ComputeThinV);
    double epsilon = std::numeric_limits<double>::epsilon();
    double tolerance = epsilon * std::max(matrix.cols(), matrix.rows()) *svd.singularValues().array().abs()(0);
    inverse_matrix = svd.matrixV() * (svd.singularValues().array().abs() > tolerance).select(svd.singularValues().array().inverse(), 0).matrix().asDiagonal() * svd.matrixU().adjoint();
    return inverse_matrix;
}*/

Eigen::VectorXd NewtonSolver::calculateSystemVector(const Eigen::VectorXd &value){
    Eigen::VectorXd system_vector(equations_ptrs_.size());
    if(!equations_ptrs_.empty()){
        for(unsigned int i = 0; i < equations_ptrs_.size(); i++){
            system_vector.coeffRef(i) = (*equations_ptrs_.at(i))(value);
        }
    }else{
        system_vector.resize(equations_.size());
        for(unsigned int i = 0; i < equations_.size(); i++){
            system_vector.coeffRef(i) = (*equations_.at(i))(value);
        }
    }
    return system_vector;
}

Eigen::VectorXd NewtonSolver::newtonMethod(const Eigen::VectorXd &initial_value) {
    Eigen::VectorXd xk_current;
    Eigen::VectorXd xk_next;
    Eigen::VectorXd error_vector;
    double error;
    double relative_error;
    Jacobian jacobian;

    if(!equations_ptrs_.empty()){
        jacobian.setEquations(equations_ptrs_);
    }else{
        jacobian.setEquations(equations_);
    }

    Eigen::MatrixXd jacobian_matrix;
    Eigen::MatrixXd inverse_jacobian_matrix;
    Eigen::VectorXd system_vector;
    xk_current.resize(initial_value.size());
    xk_current = initial_value;
    for(unsigned int i = 0; i < max_iteration_; i++){
        jacobian_matrix = jacobian.calculateJacobianMatrix(xk_current);
        inverse_jacobian_matrix = pseudoInverse(jacobian_matrix);
        system_vector = calculateSystemVector(xk_current);
        xk_next = xk_current - inverse_jacobian_matrix*system_vector;
        error_vector = xk_next - xk_current;
        error = error_vector.norm();
        relative_error = error/(xk_next.norm());
        xk_current = xk_next;
        if(error < error_tolerance_ || relative_error < error_tolerance_ || system_vector.norm() < epsilon_)
            break;
    }

    Eigen::VectorXd result(xk_current.size());
    result = xk_current;
    return result;
}

void NewtonSolver::setEquation(const EquationPtr equation_ptr) {
    equations_ptrs_.push_back(equation_ptr);
}

void NewtonSolver::setEquation(const std::shared_ptr<Equation> equation) {
    equations_.push_back(equation);
}

void NewtonSolver::setEquations(const boost::container::vector<EquationPtr> &equations_ptrs) {
    for(unsigned int i = 0; i < equations_ptrs.size(); i++){
        equations_ptrs_.push_back(equations_ptrs.at(i));
    }
}

void NewtonSolver::setEquations(const boost::container::vector<std::shared_ptr<Equation>> &equations) {
    for(unsigned int i = 0; i < equations.size(); i++){
        equations_.push_back(equations.at(i));
    }
}

Eigen::VectorXd NewtonSolver::solveEquation(const Eigen::VectorXd &initial_value) {
    return newtonMethod(initial_value);
}

void NewtonSolver::resetEquations() {
    equations_ptrs_.clear();
    equations_.clear();
}

NewtonSolver::~NewtonSolver() {}

UnconstrainedNewtonSolver::UnconstrainedNewtonSolver() : NewtonSolver() {}

UnconstrainedNewtonSolver::UnconstrainedNewtonSolver(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs) : NewtonSolver() {
    for(unsigned int i = 0; i < kinematic_equations_ptrs.size(); i++){
        kinematic_equations_ptrs_.push_back(kinematic_equations_ptrs.at(i));
    }
}

UnconstrainedNewtonSolver::UnconstrainedNewtonSolver(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations) : NewtonSolver() {
    for(unsigned int i = 0; i < kinematic_equations.size(); i++){
        kinematic_equations_.push_back(kinematic_equations.at(i));
    }
}

UnconstrainedNewtonSolver::UnconstrainedNewtonSolver(unsigned int max_iteration, double delta, double epsilon)
    : NewtonSolver(max_iteration, delta, epsilon) {}

UnconstrainedNewtonSolver::UnconstrainedNewtonSolver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs)
    : NewtonSolver(max_iteration, delta, epsilon) {
    for(unsigned int i = 0; i < kinematic_equations_ptrs.size(); i++){
        kinematic_equations_ptrs_.push_back(kinematic_equations_ptrs.at(i));
    }
}

UnconstrainedNewtonSolver::UnconstrainedNewtonSolver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations)
    : NewtonSolver(max_iteration, delta, epsilon) {
    for(unsigned int i = 0; i < kinematic_equations.size(); i++){
        kinematic_equations_.push_back(kinematic_equations.at(i));
    }
}

Eigen::VectorXd UnconstrainedNewtonSolver::calculateSystemVector(const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate) {
    Eigen::VectorXd system_vector(kinematic_equations_ptrs_.size());
    if(!kinematic_equations_ptrs_.empty()){
        for(unsigned int i = 0; i < kinematic_equations_ptrs_.size(); i++){
            system_vector.coeffRef(i) = (*kinematic_equations_ptrs_.at(i))(control_value, tip_coordinate);
        }
    }else{
        system_vector.resize(kinematic_equations_.size());
        for(unsigned int i = 0; i < kinematic_equations_.size(); i++){
            system_vector.coeffRef(i) = (*kinematic_equations_.at(i))(control_value, tip_coordinate);
        }
    }
    return system_vector;
}

Eigen::VectorXd UnconstrainedNewtonSolver::newtonMethod(bool forward, const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate) {
    Eigen::VectorXd xk_current;
    Eigen::VectorXd xk_next;
    Eigen::VectorXd error_vector;
    double error;
    double relative_error;
    UnconstrainedJacobian jacobian;
    if(!kinematic_equations_ptrs_.empty()){
        jacobian.setKinematicEquations(kinematic_equations_ptrs_);
    }else{
        jacobian.setKinematicEquations(kinematic_equations_);
    }

    Eigen::MatrixXd jacobian_matrix;
    Eigen::MatrixXd inverse_jacobian_matrix;
    Eigen::VectorXd system_vector;

    if(forward){
        xk_current.resize(tip_coordinate.size());
        xk_current = tip_coordinate;
        for(unsigned int i = 0; i < max_iteration_; i++){
            jacobian_matrix = jacobian.calculateForwardKinematicJacobianMatrix(control_value, xk_current);
            inverse_jacobian_matrix = pseudoInverse(jacobian_matrix);
            system_vector = calculateSystemVector(control_value, xk_current);
            xk_next = xk_current - inverse_jacobian_matrix*system_vector;
            error_vector = xk_next - xk_current;
            error = error_vector.norm();
            relative_error = error/(xk_next.norm());
            xk_current = xk_next;
            if(error < error_tolerance_ || relative_error < error_tolerance_ || system_vector.norm() < epsilon_)
                break;
        }
    }else{
        xk_current.resize(control_value.size());
        xk_current = control_value;
        for(unsigned int i = 0; i < max_iteration_; i++){
            jacobian_matrix = jacobian.calculateInverseKinematicJacobianMatrix(xk_current, tip_coordinate);
            inverse_jacobian_matrix = pseudoInverse(jacobian_matrix);
            system_vector = calculateSystemVector(xk_current, tip_coordinate);
            xk_next = xk_current - inverse_jacobian_matrix*system_vector;
            error_vector = xk_next - xk_current;
            error = error_vector.norm();
            relative_error = error/(xk_next.norm());
            xk_current = xk_next;
            if(error < error_tolerance_ || relative_error < error_tolerance_ || system_vector.norm() < epsilon_)
                break;
        }
    }

    Eigen::VectorXd result(xk_current.size());
    result = xk_current;
    return result;
}

void UnconstrainedNewtonSolver::setKinematicEquation(const KinematicEquationPtr kinematic_equation_ptr) {
    kinematic_equations_ptrs_.push_back(kinematic_equation_ptr);
}

void UnconstrainedNewtonSolver::setKinematicEquation(const std::shared_ptr<KinematicEquation> kinematic_equation) {
    kinematic_equations_.push_back(kinematic_equation);
}

void UnconstrainedNewtonSolver::setKinematicEquations(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs) {
    for(unsigned int i = 0; i < kinematic_equations_ptrs.size(); i++){
        kinematic_equations_ptrs_.push_back(kinematic_equations_ptrs.at(i));
    }
}

void UnconstrainedNewtonSolver::setKinematicEquations(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations) {
    for(unsigned int i = 0; i < kinematic_equations.size(); i++){
        kinematic_equations_.push_back(kinematic_equations.at(i));
    }
}

Eigen::VectorXd UnconstrainedNewtonSolver::solveForwardKinematic(const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate) {
    return newtonMethod(true, planned_control_value, current_tip_coordinate);
}

Eigen::VectorXd UnconstrainedNewtonSolver::solveInverseKinematic(const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate) {
    return newtonMethod(false, current_control_value, planned_tip_coordinate);
}

void UnconstrainedNewtonSolver::resetEquations() {
    kinematic_equations_ptrs_.clear();
    kinematic_equations_.clear();
}

UnconstrainedNewtonSolver::~UnconstrainedNewtonSolver() {}

ConstrainedNewtonSolver::ConstrainedNewtonSolver() : UnconstrainedNewtonSolver() {}

ConstrainedNewtonSolver::ConstrainedNewtonSolver(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs)
    : UnconstrainedNewtonSolver(kinematic_equations_ptrs) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs.size(); i++){
        lagrangian_auxiliary_equations_ptrs_.push_back(lagrangian_auxiliary_equations_ptrs.at(i));
    }
}

ConstrainedNewtonSolver::ConstrainedNewtonSolver(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations)
    : UnconstrainedNewtonSolver(kinematic_equations) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations.size(); i++){
        lagrangian_auxiliary_equations_.push_back(lagrangian_auxiliary_equations.at(i));
    }
}

ConstrainedNewtonSolver::ConstrainedNewtonSolver(unsigned int max_iteration, double delta, double epsilon) : UnconstrainedNewtonSolver(max_iteration, delta, epsilon) {}

ConstrainedNewtonSolver::ConstrainedNewtonSolver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs)
    : UnconstrainedNewtonSolver(max_iteration, delta, epsilon, kinematic_equations_ptrs) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs.size(); i++){
        lagrangian_auxiliary_equations_ptrs_.push_back(lagrangian_auxiliary_equations_ptrs.at(i));
    }
}

ConstrainedNewtonSolver::ConstrainedNewtonSolver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations)
    : UnconstrainedNewtonSolver(max_iteration, delta, epsilon, kinematic_equations) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations.size(); i++){
        lagrangian_auxiliary_equations_.push_back(lagrangian_auxiliary_equations.at(i));
    }
}

Eigen::VectorXd ConstrainedNewtonSolver::calculateSystemVector(const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &multiplier) {
    Eigen::VectorXd system_vector(kinematic_equations_ptrs_.size()+lagrangian_auxiliary_equations_ptrs_.size());
    if(!kinematic_equations_ptrs_.empty()){
        for(unsigned int i = 0; i < kinematic_equations_ptrs_.size(); i++){
            system_vector.coeffRef(i) = (*kinematic_equations_ptrs_.at(i))(control_value, tip_coordinate);
        }
        for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs_.size(); i++){
            system_vector.coeffRef(kinematic_equations_ptrs_.size()+i) = (*lagrangian_auxiliary_equations_ptrs_.at(i))(control_value, tip_coordinate, multiplier);
        }
    }else{
        system_vector.resize(kinematic_equations_.size()+lagrangian_auxiliary_equations_.size());
        for(unsigned int i = 0; i < kinematic_equations_.size(); i++){
            system_vector.coeffRef(i) = (*kinematic_equations_.at(i))(control_value, tip_coordinate);
        }
        for(unsigned int i = 0; i < lagrangian_auxiliary_equations_.size(); i++){
            system_vector.coeffRef(kinematic_equations_.size()+i) = (*lagrangian_auxiliary_equations_.at(i))(control_value, tip_coordinate, multiplier);
        }
    }
    return system_vector;
}

Eigen::VectorXd ConstrainedNewtonSolver::newtonMethod(bool forward, const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &multiplier) {
    Eigen::VectorXd xk_current;
    Eigen::VectorXd xk_current_value;
    Eigen::VectorXd xk_current_multiplier;
    Eigen::VectorXd xk_next;
    Eigen::VectorXd error_vector;
    double error;
    double relative_error;
    ConstrainedJacobian jacobian;

    if(!kinematic_equations_ptrs_.empty()){
        jacobian.setKinematicAndLagrangianAuxiliaryEquations(kinematic_equations_ptrs_, lagrangian_auxiliary_equations_ptrs_);
    }else{
        jacobian.setKinematicAndLagrangianAuxiliaryEquations(kinematic_equations_, lagrangian_auxiliary_equations_);
    }

    Eigen::MatrixXd jacobian_matrix;
    Eigen::MatrixXd inverse_jacobian_matrix;
    Eigen::VectorXd system_vector;
    if(forward){
        xk_current.resize(tip_coordinate.size()+multiplier.size());
        xk_current << tip_coordinate, multiplier;
        xk_current_value = tip_coordinate;
        xk_current_multiplier = multiplier;
        for(unsigned int i = 0; i < max_iteration_; i++){
            jacobian_matrix = jacobian.calculateForwardKinematicJacobianMatrix(control_value, xk_current_value, xk_current_multiplier);
            inverse_jacobian_matrix = pseudoInverse(jacobian_matrix);
            system_vector = calculateSystemVector(control_value, xk_current_value, xk_current_multiplier);
            xk_next = xk_current - inverse_jacobian_matrix*system_vector;
            error_vector = xk_next - xk_current;
            error = error_vector.norm();
            relative_error = error/(xk_next.norm());
            xk_current = xk_next;
            if(error < error_tolerance_ || relative_error < error_tolerance_ || system_vector.norm() < epsilon_)
                break;
            xk_current_value = xk_current.segment(0, xk_current_value.size());
            xk_current_multiplier = xk_current.segment(xk_current_value.size(), xk_current_multiplier.size());
        }
    }else{
        xk_current.resize(control_value.size()+multiplier.size());
        xk_current << control_value, multiplier;
        xk_current_value = control_value;
        xk_current_multiplier = multiplier;
        for(unsigned int i = 0; i < max_iteration_; i++){
            jacobian_matrix = jacobian.calculateInverseKinematicJacobianMatrix(xk_current_value, tip_coordinate, xk_current_multiplier);
            inverse_jacobian_matrix = pseudoInverse(jacobian_matrix);
            system_vector = calculateSystemVector(xk_current_value, tip_coordinate, xk_current_multiplier);
            xk_next = xk_current - inverse_jacobian_matrix*system_vector;
            error_vector = xk_next - xk_current;
            error = error_vector.norm();
            relative_error = error/(xk_next.norm());
            xk_current = xk_next;
            if(error < error_tolerance_ || relative_error < error_tolerance_ || system_vector.norm() < epsilon_)
                break;
            xk_current_value = xk_current.segment(0, xk_current_value.size());
            xk_current_multiplier = xk_current.segment(xk_current_value.size(), xk_current_multiplier.size());
        }
    }

    Eigen::VectorXd result(xk_current_value.size());
    result = xk_current_value;
    return result;
}

void ConstrainedNewtonSolver::setLagrangianAuxiliaryEquation(const LagrangianAuxiliaryEquationPtr lagrangian_auxiliary_equation_ptr) {
    lagrangian_auxiliary_equations_ptrs_.push_back(lagrangian_auxiliary_equation_ptr);
}

void ConstrainedNewtonSolver::setLagrangianAuxiliaryEquation(const std::shared_ptr<LagrangianAuxiliaryEquation> lagrangian_auxiliary_equation) {
    lagrangian_auxiliary_equations_.push_back(lagrangian_auxiliary_equation);
}

void ConstrainedNewtonSolver::setLagrangianAuxiliaryEquations(const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs.size(); i++){
        lagrangian_auxiliary_equations_ptrs_.push_back(lagrangian_auxiliary_equations_ptrs.at(i));
    }
}

void ConstrainedNewtonSolver::setLagrangianAuxiliaryEquations(const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations.size(); i++){
        lagrangian_auxiliary_equations_.push_back(lagrangian_auxiliary_equations.at(i));
    }
}

void ConstrainedNewtonSolver::setKinematicAndLagrangianAuxiliaryEquations(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs) {
    for(unsigned int i = 0; i < kinematic_equations_ptrs.size(); i++){
        kinematic_equations_ptrs_.push_back(kinematic_equations_ptrs.at(i));
    }
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs.size(); i++){
        lagrangian_auxiliary_equations_ptrs_.push_back(lagrangian_auxiliary_equations_ptrs.at(i));
    }
}

void ConstrainedNewtonSolver::setKinematicAndLagrangianAuxiliaryEquations(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations) {
    for(unsigned int i = 0; i < kinematic_equations.size(); i++){
        kinematic_equations_.push_back(kinematic_equations.at(i));
    }
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations.size(); i++){
        lagrangian_auxiliary_equations_.push_back(lagrangian_auxiliary_equations.at(i));
    }
}

Eigen::VectorXd ConstrainedNewtonSolver::solveForwardKinematic(const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate, const Eigen::VectorXd &multiplier) {
    return newtonMethod(true, planned_control_value, current_tip_coordinate, multiplier);
}

Eigen::VectorXd ConstrainedNewtonSolver::solveInverseKinematic(const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate, const Eigen::VectorXd &multiplier) {
    return newtonMethod(false, current_control_value, planned_tip_coordinate, multiplier);
}

void ConstrainedNewtonSolver::resetEquations() {
    UnconstrainedNewtonSolver::resetEquations();
    lagrangian_auxiliary_equations_ptrs_.clear();
    lagrangian_auxiliary_equations_.clear();
}

ConstrainedNewtonSolver::~ConstrainedNewtonSolver() {}

GradientSolver::GradientSolver()
    : Solver() {
        initial_step_size_ = InitialStepSize::SIZE_DIGITS_1;
        objective_function_ptr_ = nullptr;
        objective_function_.reset();
    }

GradientSolver::GradientSolver(const ObjectiveFunctionPtr objective_function_ptr)
    : Solver() {
    initial_step_size_ = InitialStepSize::SIZE_DIGITS_1;
    objective_function_ptr_ = objective_function_ptr;
    objective_function_.reset();
}

GradientSolver::GradientSolver(const std::shared_ptr<ObjectiveFunction> objective_function)
    : Solver() {
    initial_step_size_ = InitialStepSize::SIZE_DIGITS_1;
    objective_function_ = objective_function;
    objective_function_ptr_ = nullptr;
}

GradientSolver::GradientSolver(unsigned int max_iteration, double error_tolerance, double epsilon, double initial_step_size)
    : Solver(max_iteration, error_tolerance, epsilon) {
        initial_step_size_ = initial_step_size;
    }

GradientSolver::GradientSolver(unsigned int max_iteration, double error_tolerance, double epsilon, double initial_step_size, const ObjectiveFunctionPtr objective_function_ptr)
    : Solver(max_iteration, error_tolerance, epsilon) {
    initial_step_size_ = initial_step_size;
    objective_function_ptr_ = objective_function_ptr;
    objective_function_.reset();
}

GradientSolver::GradientSolver(unsigned int max_iteration, double error_tolerance, double epsilon, double initial_step_size, const std::shared_ptr<ObjectiveFunction> objective_function)
    : Solver(max_iteration, error_tolerance, epsilon) {
    initial_step_size_ = initial_step_size;
    objective_function_ = objective_function;
    objective_function_ptr_ = nullptr;
}

const double GradientSolver::derivative(const std::shared_ptr<ObjectivePhi> &objective_phi, const double abscissa_value) {
    boost::container::vector<double> step_sizes;
    boost::container::vector<double> approximate_derivatives;
    boost::container::vector<double> error_bounds;
    boost::container::vector<double> relative_errors;

    double step_size = initial_step_size_;
    double increase_abscissa_value = abscissa_value;
    double decrease_abscissa_value = abscissa_value;
    increase_abscissa_value = abscissa_value + step_size;
    decrease_abscissa_value = abscissa_value - step_size;
    double error_bound;
    double approximate_value;
    step_sizes.push_back(step_size);
    approximate_derivatives.push_back(((*objective_phi)(increase_abscissa_value) - (*objective_phi)(decrease_abscissa_value)) / (2 * step_size));
    error_bounds.push_back(0);
    relative_errors.push_back(0);

    for(unsigned int i = 0; i < 2; i++){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_abscissa_value = abscissa_value + step_size;
        decrease_abscissa_value = abscissa_value - step_size;
        approximate_value = ((*objective_phi)(increase_abscissa_value) - (*objective_phi)(decrease_abscissa_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*objective_phi)(increase_abscissa_value) - (*objective_phi)(decrease_abscissa_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+1]) + std::abs(approximate_derivatives[i])));
    }

    unsigned int i = 1;
    while(error_bounds[i] > error_bounds[i+1] && relative_errors[i] > error_tolerance_ && i < max_iteration_){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_abscissa_value = abscissa_value + step_size;
        decrease_abscissa_value = abscissa_value - step_size;
        approximate_value = ((*objective_phi)(increase_abscissa_value) - (*objective_phi)(decrease_abscissa_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*objective_phi)(increase_abscissa_value) - (*objective_phi)(decrease_abscissa_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+2]) + std::abs(approximate_derivatives[i+1])));
        i++;
    }
    return approximate_derivatives[i];
}

void GradientSolver::bracketInterval(const std::shared_ptr<ObjectivePhi> &objective_phi, double &lower_abscissa_value, double &upper_abscissa_value, double &middle_abscissa_value) {
    double phi_lower_value = (*objective_phi)(lower_abscissa_value);
    double phi_upper_value = (*objective_phi)(upper_abscissa_value);
    //std::cout << "phi_lower " << phi_lower_value << std::endl;
    //std::cout << "phi_upper " << phi_upper_value << std::endl;
    middle_abscissa_value = middle_abscissa_value + boost::math::constants::phi<double>() * (middle_abscissa_value - lower_abscissa_value);
    double phi_middle_value = (*objective_phi)(middle_abscissa_value);

    while(phi_upper_value > phi_middle_value){
        lower_abscissa_value = upper_abscissa_value;
        phi_lower_value = phi_upper_value;
        upper_abscissa_value = middle_abscissa_value;
        phi_upper_value = phi_middle_value;
        middle_abscissa_value = middle_abscissa_value + boost::math::constants::phi<double>() * (middle_abscissa_value - lower_abscissa_value);
        phi_middle_value = (*objective_phi)(middle_abscissa_value);
    }
}

const double GradientSolver::partialDerivative(const ObjectiveFunctionPtr objective_function_ptr, const unsigned int partial_variable_index, const Eigen::VectorXd &abscissa_value) {
    boost::container::vector<double> step_sizes;
    boost::container::vector<double> approximate_derivatives;
    boost::container::vector<double> error_bounds;
    boost::container::vector<double> relative_errors;

    double step_size = initial_step_size_;
    Eigen::VectorXd increase_marginal_value = abscissa_value;
    Eigen::VectorXd decrease_marginal_value = abscissa_value;
    double marginal_value = abscissa_value[partial_variable_index];
    increase_marginal_value[partial_variable_index] = marginal_value + step_size;
    decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
    double error_bound;
    double approximate_value;
    step_sizes.push_back(step_size);
    approximate_derivatives.push_back(((*objective_function_ptr)(increase_marginal_value) - (*objective_function_ptr)(decrease_marginal_value)) / (2 * step_size));
    error_bounds.push_back(0);
    relative_errors.push_back(0);

    for(unsigned int i = 0; i < 2; i++){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*objective_function_ptr)(increase_marginal_value) - (*objective_function_ptr)(decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*objective_function_ptr)(increase_marginal_value) - (*objective_function_ptr)(decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+1]) + std::abs(approximate_derivatives[i])));
    }

    unsigned int i = 1;
    while(error_bounds[i] > error_bounds[i+1] && relative_errors[i] > error_tolerance_ && i < max_iteration_){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*objective_function_ptr)(increase_marginal_value) - (*objective_function_ptr)(decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*objective_function_ptr)(increase_marginal_value) - (*objective_function_ptr)(decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+2]) + std::abs(approximate_derivatives[i+1])));
        i++;
    }
    return approximate_derivatives[i];
}

const double GradientSolver::partialDerivative(const std::shared_ptr<ObjectiveFunction> &objective_function, const unsigned int partial_variable_index, const Eigen::VectorXd &abscissa_value) {
    boost::container::vector<double> step_sizes;
    boost::container::vector<double> approximate_derivatives;
    boost::container::vector<double> error_bounds;
    boost::container::vector<double> relative_errors;

    double step_size = initial_step_size_;
    Eigen::VectorXd increase_marginal_value = abscissa_value;
    Eigen::VectorXd decrease_marginal_value = abscissa_value;
    double marginal_value = abscissa_value[partial_variable_index];
    increase_marginal_value[partial_variable_index] = marginal_value + step_size;
    decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
    double error_bound;
    double approximate_value;
    step_sizes.push_back(step_size);
    approximate_derivatives.push_back(((*objective_function)(increase_marginal_value) - (*objective_function)(decrease_marginal_value)) / (2 * step_size));
    error_bounds.push_back(0);
    relative_errors.push_back(0);

    for(unsigned int i = 0; i < 2; i++){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*objective_function)(increase_marginal_value) - (*objective_function)(decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*objective_function)(increase_marginal_value) - (*objective_function)(decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+1]) + std::abs(approximate_derivatives[i])));
    }

    unsigned int i = 1;
    while(error_bounds[i] > error_bounds[i+1] && relative_errors[i] > error_tolerance_ && i < max_iteration_){
        step_size = step_size / 10;
        step_sizes.push_back(step_size);
        increase_marginal_value[partial_variable_index] = marginal_value + step_size;
        decrease_marginal_value[partial_variable_index] = marginal_value - step_size;
        approximate_value = ((*objective_function)(increase_marginal_value) - (*objective_function)(decrease_marginal_value)) / (2 * step_size);
        approximate_derivatives.push_back(((*objective_function)(increase_marginal_value) - (*objective_function)(decrease_marginal_value)) / (2 * step_size));
        error_bound = std::abs(approximate_derivatives[i+1] - approximate_derivatives[i]);
        error_bounds.push_back(error_bound);
        relative_errors.push_back((2*error_bound)/(std::abs(approximate_derivatives[i+2]) + std::abs(approximate_derivatives[i+1])));
        i++;
    }
    return approximate_derivatives[i];
}

Eigen::VectorXd GradientSolver::calculateGradient(const Eigen::VectorXd &abscissa_value) {
    Eigen::VectorXd gradient(abscissa_value.size());
    if(objective_function_ptr_ != nullptr){
        for(unsigned int i = 0; i < abscissa_value.size(); i++){
            gradient(i) = partialDerivative(objective_function_ptr_, i, abscissa_value);
        }
    }else{
        for(unsigned int i = 0; i < abscissa_value.size(); i++){
            gradient(i) = partialDerivative(objective_function_, i, abscissa_value);
        }
    }
    return gradient;
}

double GradientSolver::quadraticMinimization(const std::shared_ptr<ObjectivePhi> &objective_phi, double &lower_abscissa_value, double &upper_abscissa_value) {
    int bits = std::numeric_limits<double>::digits;
    std::pair<double, double> r = boost::math::tools::brent_find_minima(*objective_phi.get(), lower_abscissa_value, upper_abscissa_value, bits);
    return r.first;
}

void GradientSolver::setEquation(const ObjectiveFunctionPtr objective_function_ptr) {
    objective_function_ptr_ = objective_function_ptr;
}

void GradientSolver::setEquation(const std::shared_ptr<ObjectiveFunction> &objective_function) {
    objective_function_ = objective_function;
}

Eigen::VectorXd GradientSolver::minimizeObjectiveFunction(const Eigen::VectorXd &initial_value) {
    unsigned int i = 0;
    Eigen::VectorXd p_current = initial_value;
    Eigen::VectorXd p_next;
    Eigen::VectorXd s_current;
    Eigen::VectorXd gradient;

    std::shared_ptr<ObjectivePhi> objective_phi = std::make_shared<ObjectivePhi>(objective_function_);

    double gamma;
    double interval_lower_bound = 0;
    double interval_upper_bound = 1;
    double interval_middle_bound;
    double error = 1;
    double relative_error = 1;
    while(i < max_iteration_ && relative_error > error_tolerance_ && error > error_tolerance_){
        gradient = calculateGradient(p_current);
        s_current = -gradient/gradient.norm();
        objective_phi->setObjectiveFunctionPkSk(p_current, s_current);
        bracketInterval(objective_phi, interval_lower_bound, interval_upper_bound, interval_middle_bound);
        gamma = quadraticMinimization(objective_phi, interval_lower_bound, interval_upper_bound);
        p_next = p_current + gamma*s_current;
        error = std::abs(((* objective_function_)(p_next) - (* objective_function_)(p_current)));
        relative_error = std::abs(error/(* objective_function_)(p_current));
        p_current = p_next;
        i++;
    }
    return p_current;
}

void GradientSolver::resetObjectiveFunction() {
    objective_function_ptr_ = nullptr;
    objective_function_.reset();
}

GradientSolver::~GradientSolver() {}
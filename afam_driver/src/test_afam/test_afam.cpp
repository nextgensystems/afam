#include <ros/ros.h>
#include "afam_driver/afam.h"


int main(int argc, char **argv) {
    ros::init(argc, argv, "afam_1");
    AFAM afam(argc, argv, "afam_1");

    std::cout.precision(std::numeric_limits<double>::max_digits10);

    Eigen::Vector3d origin_tip_coordinate;
    origin_tip_coordinate(0) = 1956.8304876913846;
    origin_tip_coordinate(1) = -7.2511879889557046e-13;
    origin_tip_coordinate(2) = 936.9999999999975;

    Eigen::Vector3d tip_coordinate_1;
    tip_coordinate_1(0) = 1640.31;
    tip_coordinate_1(1) = 200.701;
    tip_coordinate_1(2) = 2295.33;
    

    Eigen::Vector3d tip_coordinate_2;
    tip_coordinate_2(0) = 1975.52;
    tip_coordinate_2(1) = 27.9134;
    tip_coordinate_2(2) = 927.825;

    Eigen::Vector3d tip_coordinate_3;
    tip_coordinate_3(0) = 1975.52;
    tip_coordinate_3(1) = 88.0588;
    tip_coordinate_3(2) = 1058.35;

    Eigen::Vector3d tip_coordinate_4;
    tip_coordinate_4(0) = 1465.01;
    tip_coordinate_4(1) = 507.9134;
    tip_coordinate_4(2) = 427.825;

    Eigen::Vector4d control_value_1;
    control_value_1(0) = 0;
    control_value_1(1) = 0;
    control_value_1(2) = 0;
    control_value_1(3) = 0;

    Eigen::Vector4d control_value_2;
    control_value_2(0) = 10;
    control_value_2(1) = 0;
    control_value_2(2) = 0;
    control_value_2(3) = 0;

    /*std::cout << "afam name: " << afam.getName() << std::endl;

    Eigen::Vector4d control_value = afam.calculateControlValue(next_tip_coordinate);

    std::cout << "control value" << std::endl;
    std::cout << control_value << std::endl;
    afam.movingTipToNextCoordinate(next_tip_coordinate);*/

    Eigen::Vector4d control_value;
    control_value(0) = 10;
    control_value(1) = -10;
    control_value(2) = 10;
    control_value(3) = 10;

    //afam.movingTipToNextCoordinate(control_value);

    ros::Rate r(0.1);
    Eigen::Vector3d tip_coordinate;
    //Eigen::Vector4d control_value;

    /*ros::NodeHandle node_handle;
    ros::Publisher marker_publisher = node_handle.advertise<visualization_msgs::Marker>("visualization_marker", 1000);*/
    visualization_msgs::Marker workbench;


    while(1){
        control_value(0) = 0;
        control_value(1) = 0;
        control_value(2) = 0;
        control_value(3) = 0;

        //forward
        for(unsigned int i = 0; i < 5000; i++){
            control_value(0) += 0.1;
            control_value(2) += 0.1;
            std::cout << "control_value" << std::endl;
            std::cout << control_value << std::endl;
            tip_coordinate = afam.movingTipToNextCoordinate(control_value);
            std::cout << "tip_coordinate" << std::endl;
            std::cout << tip_coordinate << std::endl;
            //r.sleep();
        }

        //left
        for(unsigned int i = 0; i < 5000; i++){
            control_value(1) -= 0.1;
            control_value(3) += 0.1;
            std::cout << "control_value" << std::endl;
            std::cout << control_value << std::endl;
            tip_coordinate = afam.movingTipToNextCoordinate(control_value);
            std::cout << "tip_coordinate" << std::endl;
            std::cout << tip_coordinate << std::endl;
            //r.sleep();
        }

        //down
        for(unsigned int i = 0; i < 1000; i++){
            control_value(2) += 0.1;
            std::cout << "control_value" << std::endl;
            std::cout << control_value << std::endl;
            tip_coordinate = afam.movingTipToNextCoordinate(control_value);
            std::cout << "tip_coordinate" << std::endl;
            std::cout << tip_coordinate << std::endl;
            //r.sleep();
        }

        //up
        for(unsigned int i = 0; i < 1000; i++){
            control_value(2) -= 0.1;
            std::cout << "control_value" << std::endl;
            std::cout << control_value << std::endl;
            tip_coordinate = afam.movingTipToNextCoordinate(control_value);
            std::cout << "tip_coordinate" << std::endl;
            std::cout << tip_coordinate << std::endl;
            //r.sleep();
        }

        //right
        for(unsigned int i = 0; i < 5000; i++){
            //control_value(0) += 0.01;
            control_value(1) += 0.1;
            //control_value(2) += 0.05;
            control_value(3) -= 0.1;
            std::cout << "control_value" << std::endl;
            std::cout << control_value << std::endl;
            tip_coordinate = afam.movingTipToNextCoordinate(control_value);
            std::cout << "tip_coordinate" << std::endl;
            std::cout << tip_coordinate << std::endl;
            //r.sleep();
        }

        //down
        for(unsigned int i = 0; i < 1000; i++){
            //control_value(0) -= 0.01;
            //control_value(1) += 0.01;
            control_value(2) += 0.1;
            //control_value(3) += 0.01;
            std::cout << "control_value" << std::endl;
            std::cout << control_value << std::endl;
            tip_coordinate = afam.movingTipToNextCoordinate(control_value);
            std::cout << "tip_coordinate" << std::endl;
            std::cout << tip_coordinate << std::endl;
            //r.sleep();
        }

        //up
        for(unsigned int i = 0; i < 1000; i++){
            //control_value(0) -= 0.01;
            //control_value(1) += 0.01;
            control_value(2) -= 0.1;
            //control_value(3) += 0.01;
            std::cout << "control_value" << std::endl;
            std::cout << control_value << std::endl;
            tip_coordinate = afam.movingTipToNextCoordinate(control_value);
            std::cout << "tip_coordinate" << std::endl;
            std::cout << tip_coordinate << std::endl;
            //r.sleep();
        }

        //backward
        for(unsigned int i = 0; i < 5000; i++){
            control_value(0) -= 0.1;
            control_value(2) -= 0.1;
            std::cout << "control_value" << std::endl;
            std::cout << control_value << std::endl;
            tip_coordinate = afam.movingTipToNextCoordinate(control_value);
            std::cout << "tip_coordinate" << std::endl;
            std::cout << tip_coordinate << std::endl;
        }
        afam.resetState();
        r.sleep();
    }

    return 0;
}

#include "afam_driver/afam.h"


AFAM::AFAM(int &argc, char **argv, const std::string &afam_name)
    :afam_name_(afam_name),
     a0_(1610),
     //b0_(0.00072717),
     c0_(315),
     d0_(346.830487691385),
     h0_(1252),
     l0_(7580),
     xc1_(-6538.86097649422),
     yc1_(-214.720838852867),
     xc2_(0),
     yc2_(0) {
        
    b0_ = std::sqrt(l0_*l0_ - h0_*h0_ - (yc2_-yc1_)*(yc2_-yc1_)) + xc1_ - xc2_ - d0_;
    ros::init(argc, argv, afam_name);
    afam_joint_publisher_ = node_handle_.advertise<sensor_msgs::JointState>("joint_states_afam", 1000);
    marker_publisher_ = node_handle_.advertise<visualization_msgs::Marker>("visualization_marker", 1000);
   
    state_msg_.name.resize(6);
    state_msg_.position.resize(6);

    state_msg_.name[0] = "q0";
    state_msg_.name[1] = "q1";
    state_msg_.name[2] = "q2";
    state_msg_.name[3] = "q3";
    state_msg_.name[4] = "q4";
    state_msg_.name[5] = "q5";

    state_msg_.position[0] = 0.0;
    state_msg_.position[1] = 0.0;
    state_msg_.position[2] = 0.0;
    state_msg_.position[3] = 0.0;
    state_msg_.position[4] = 0.0;
    state_msg_.position[5] = 0.0;

    points_.header.frame_id = "base_link";
    points_.ns = "points_and_lines";
    points_.action = visualization_msgs::Marker::ADD;
    points_.pose.orientation.w = 1.0;
    points_.id = 0;
    points_.scale.x = 0.00002;
    points_.scale.y = 0.00002;
    points_.color.g = 1.0f;
    points_.color.a = 1.0;
    points_.type = visualization_msgs::Marker::POINTS;



    current_control_value_.setZero();

    current_revolute_arm_joint_coordinate_(0) = xc2_;      // xr, yr, zr
    current_revolute_arm_joint_coordinate_(1) = yc2_;
    current_revolute_arm_joint_coordinate_(2) = h0_;

    current_cable_attaching_coordinate_(0) = xc2_+d0_+b0_; // xb, yb, zb
    current_cable_attaching_coordinate_(1) = yc2_;
    current_cable_attaching_coordinate_(2) = h0_;

    current_end_coordinate_(0) = xc2_+d0_+a0_;             // xt, yt, zt
    current_end_coordinate_(1) = yc2_;
    current_end_coordinate_(2) = h0_;

    current_tip_coordinate_(0) = xc2_+d0_+a0_;             // x, y, z
    current_tip_coordinate_(1) = yc2_;
    current_tip_coordinate_(2) = h0_-c0_;

    current_control_value_(0) = 0;
    current_control_value_(1) = 0;
    current_control_value_(2) = 0;
    current_control_value_(3) = 0;

    lambda_.setZero();

    Eigen::VectorXd constants(10);
    constants(0) = a0_;
    constants(1) = b0_;
    constants(2) = c0_;
    constants(3) = d0_;
    constants(4) = h0_;
    constants(5) = l0_;
    constants(6) = xc1_;
    constants(7) = yc1_;
    constants(8) = xc2_;
    constants(9) = yc2_;

    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation1>(constants)));
    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation2>(constants)));
    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation3>(constants)));
    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation4>(constants)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation1>(constants)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation2>(constants)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation3>(constants)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation4>(constants)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation5>(constants)));

    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation1>(constants)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation2>(constants)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation3>(constants)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation4>(constants)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation5>(constants)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation6>(constants)));

    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv1>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv2>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv3>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv4>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv5>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv6>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv7>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv8>(constants)));

}

AFAM::AFAM(int &argc, char **argv, const std::string &afam_name, const double a0, const double b0, const double c0, const double d0, const double h0, const double l0, const double xc1, const double yc1, const double xc2, const double yc2)
    :afam_name_(afam_name),
     a0_(a0),
     b0_(b0),
     c0_(c0),
     d0_(d0),
     h0_(h0),
     l0_(l0),
     xc1_(xc1),
     yc1_(yc1),
     xc2_(xc2),
     yc2_(yc2) {

    ros::init(argc, argv, afam_name);
    afam_joint_publisher_ = node_handle_.advertise<sensor_msgs::JointState>("afam_joint_states", 1000);
    marker_publisher_ = node_handle_.advertise<visualization_msgs::Marker>("visualization_marker", 1000);
   
    state_msg_.name.resize(6);
    state_msg_.position.resize(6);

    state_msg_.name[0] = "q0";
    state_msg_.name[1] = "q1";
    state_msg_.name[2] = "q2";
    state_msg_.name[3] = "q3";
    state_msg_.name[4] = "q4";
    state_msg_.name[5] = "q5";

    state_msg_.position[0] = 0.0;
    state_msg_.position[1] = 0.0;
    state_msg_.position[2] = 0.0;
    state_msg_.position[3] = 0.0;
    state_msg_.position[4] = 0.0;
    state_msg_.position[5] = 0.0;

    points_.header.frame_id = "base_link";
    points_.ns = "points_and_lines";
    points_.action = visualization_msgs::Marker::ADD;
    points_.pose.orientation.w = 1.0;
    points_.id = 0;
    points_.scale.x = 0.00002;
    points_.scale.y = 0.00002;
    points_.color.g = 1.0f;
    points_.color.a = 1.0;
    points_.type = visualization_msgs::Marker::POINTS;

    state_msg_.name.resize(6);
    state_msg_.position.resize(6);

    state_msg_.name[0] = "q0";
    state_msg_.name[1] = "q1";
    state_msg_.name[2] = "q2";
    state_msg_.name[3] = "q3";
    state_msg_.name[4] = "q4";
    state_msg_.name[5] = "q5";

    state_msg_.position[0] = 0.0;
    state_msg_.position[1] = 0.0;
    state_msg_.position[2] = 0.0;
    state_msg_.position[3] = 0.0;
    state_msg_.position[4] = 0.0;
    state_msg_.position[5] = 0.0;

    current_control_value_.setZero();

    current_revolute_arm_joint_coordinate_(0) = xc2_;      // xr, yr, zr
    current_revolute_arm_joint_coordinate_(1) = yc2_;
    current_revolute_arm_joint_coordinate_(2) = h0_;

    current_cable_attaching_coordinate_(0) = xc2_+d0_+b0_; // xb, yb, zb
    current_cable_attaching_coordinate_(1) = yc2_;
    current_cable_attaching_coordinate_(2) = h0_;

    current_end_coordinate_(0) = xc2_+d0_+a0_;             // xt, yt, zt
    current_end_coordinate_(1) = yc2_;
    current_end_coordinate_(2) = h0_;

    current_tip_coordinate_(0) = xc2_+d0_+a0_;             // x, y, z
    current_tip_coordinate_(1) = yc2_;
    current_tip_coordinate_(2) = h0_-c0_;

    current_control_value_(0) = 0;
    current_control_value_(1) = 0;
    current_control_value_(2) = 0;
    current_control_value_(3) = 0;

    lambda_.setZero();

    Eigen::VectorXd constants(10);
    constants(0) = a0_;
    constants(1) = b0_;
    constants(2) = c0_;
    constants(3) = d0_;
    constants(4) = h0_;
    constants(5) = l0_;
    constants(6) = xc1_;
    constants(7) = yc1_;
    constants(8) = xc2_;
    constants(9) = yc2_;

    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation1>(constants)));
    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation2>(constants)));
    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation3>(constants)));
    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation4>(constants)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation1>(constants)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation2>(constants)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation3>(constants)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation4>(constants)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation5>(constants)));

    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation1>(constants)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation2>(constants)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation3>(constants)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation4>(constants)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation5>(constants)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation6>(constants)));

    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv1>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv2>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv3>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv4>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv5>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv6>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv7>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv8>(constants)));

}

AFAM::AFAM(int &argc, char **argv, const std::string &afam_name, Eigen::VectorXd &constants)
    :afam_name_(afam_name),
     a0_(constants(0)),
     b0_(constants(1)),
     c0_(constants(2)),
     d0_(constants(3)),
     h0_(constants(4)),
     l0_(constants(5)),
     xc1_(constants(6)),
     yc1_(constants(7)),
     xc2_(constants(8)),
     yc2_(constants(9)) {

    ros::init(argc, argv, afam_name);
    afam_joint_publisher_ = node_handle_.advertise<sensor_msgs::JointState>("afam_joint_states", 1000);
    marker_publisher_ = node_handle_.advertise<visualization_msgs::Marker>("visualization_marker", 1000);
   
    state_msg_.name.resize(6);
    state_msg_.position.resize(6);

    state_msg_.name[0] = "q0";
    state_msg_.name[1] = "q1";
    state_msg_.name[2] = "q2";
    state_msg_.name[3] = "q3";
    state_msg_.name[4] = "q4";
    state_msg_.name[5] = "q5";

    state_msg_.position[0] = 0.0;
    state_msg_.position[1] = 0.0;
    state_msg_.position[2] = 0.0;
    state_msg_.position[3] = 0.0;
    state_msg_.position[4] = 0.0;
    state_msg_.position[5] = 0.0;

    points_.header.frame_id = "base_link";
    points_.ns = "points_and_lines";
    points_.action = visualization_msgs::Marker::ADD;
    points_.pose.orientation.w = 1.0;
    points_.id = 0;
    points_.scale.x = 0.00002;
    points_.scale.y = 0.00002;
    points_.color.g = 1.0f;
    points_.color.a = 1.0;
    points_.type = visualization_msgs::Marker::POINTS;

    
    state_msg_.name.resize(6);
    state_msg_.position.resize(6);

    state_msg_.name[0] = "q0";
    state_msg_.name[1] = "q1";
    state_msg_.name[2] = "q2";
    state_msg_.name[3] = "q3";
    state_msg_.name[4] = "q4";
    state_msg_.name[5] = "q5";

    state_msg_.position[0] = 0.0;
    state_msg_.position[1] = 0.0;
    state_msg_.position[2] = 0.0;
    state_msg_.position[3] = 0.0;
    state_msg_.position[4] = 0.0;
    state_msg_.position[5] = 0.0;

    current_control_value_.setZero();

    current_revolute_arm_joint_coordinate_(0) = xc2_;      // xr, yr, zr
    current_revolute_arm_joint_coordinate_(1) = yc2_;
    current_revolute_arm_joint_coordinate_(2) = h0_;

    current_cable_attaching_coordinate_(0) = xc2_+d0_+b0_; // xb, yb, zb
    current_cable_attaching_coordinate_(1) = yc2_;
    current_cable_attaching_coordinate_(2) = h0_;

    current_end_coordinate_(0) = xc2_+d0_+a0_;             // xt, yt, zt
    current_end_coordinate_(1) = yc2_;
    current_end_coordinate_(2) = h0_;

    current_tip_coordinate_(0) = xc2_+d0_+a0_;             // x, y, z
    current_tip_coordinate_(1) = yc2_;
    current_tip_coordinate_(2) = h0_-c0_;

    current_control_value_(0) = 0;
    current_control_value_(1) = 0;
    current_control_value_(2) = 0;
    current_control_value_(3) = 0;

    lambda_.setZero();

    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation1>(constants)));
    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation2>(constants)));
    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation3>(constants)));
    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation4>(constants)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation1>(constants)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation2>(constants)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation3>(constants)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation4>(constants)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation5>(constants)));

    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation1>(constants)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation2>(constants)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation3>(constants)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation4>(constants)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation5>(constants)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation6>(constants)));

    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv1>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv2>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv3>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv4>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv5>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv6>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv7>(constants)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv8>(constants)));

}

void AFAM::setParameters(Eigen::VectorXd &constants) {
    a0_ = constants(0);
    c0_ = constants(2);
    d0_ = constants(3);
    h0_ = constants(4);
    l0_ = constants(5);
    xc1_ = constants(6);
    yc1_ = constants(7);
    xc2_ = constants(8);
    yc2_ = constants(9);
    b0_ = std::sqrt(l0_*l0_ - h0_*h0_ - (yc2_-yc1_)*(yc2_-yc1_)) + xc1_ - xc2_ - d0_;

    Eigen::VectorXd constants_(10);
    constants_(0) = a0_;
    constants_(1) = b0_;
    constants_(2) = c0_;
    constants_(3) = d0_;
    constants_(4) = h0_;
    constants_(5) = l0_;
    constants_(6) = xc1_;
    constants_(7) = yc1_;
    constants_(8) = xc2_;
    constants_(9) = yc2_;

    solver_constrained_control_.resetEquations();
    solver_unconstrained_tip_.resetEquations();
    solver_unconstrained_tip_inv_.resetEquations();

    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation1>(constants_)));
    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation2>(constants_)));
    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation3>(constants_)));
    solver_constrained_control_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<ControlValueToCableAttachingPointKinematicEquation4>(constants_)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation1>(constants_)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation2>(constants_)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation3>(constants_)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation4>(constants_)));
    solver_constrained_control_.setLagrangianAuxiliaryEquation((std::shared_ptr<LagrangianAuxiliaryEquation>)(std::make_shared<ControlValueToCableAttachingPointLagrangianAuxiliaryEquation5>(constants_)));

    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation1>(constants_)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation2>(constants_)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation3>(constants_)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation4>(constants_)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation5>(constants_)));
    solver_unconstrained_tip_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquation6>(constants_)));

    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv1>(constants_)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv2>(constants_)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv3>(constants_)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv4>(constants_)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv5>(constants_)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv6>(constants_)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv7>(constants_)));
    solver_unconstrained_tip_inv_.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<JointToTipKinematicEquationInv8>(constants_)));
    std::cout << "a0 " << a0_ << std::endl;
    std::cout << "b0 " << b0_ << std::endl;
    std::cout << "c0 " << c0_ << std::endl;
    std::cout << "d0 " << d0_ << std::endl;
    std::cout << "h0 " << h0_ << std::endl;
    std::cout << "l0 " << l0_ << std::endl;
    std::cout << "xc1 " << xc1_ << std::endl;
    std::cout << "yc1 " << yc1_ << std::endl;
    std::cout << "xc2 " << xc2_ << std::endl;
    std::cout << "yc2 " << yc2_ << std::endl;
}

Eigen::Vector4d &AFAM::calculateControlValue(Eigen::Vector3d &next_tip_coordinate) {
    //lambda_.setZero();
    Eigen::VectorXd current_joint_cable_attaching_end_coordinates(8);
    current_joint_cable_attaching_end_coordinates << current_revolute_arm_joint_coordinate_.segment(0, 2), current_cable_attaching_coordinate_, current_end_coordinate_;
    Eigen::VectorXd next_joint_cable_attaching_end_coordinates(8);
    next_joint_cable_attaching_end_coordinates = solver_unconstrained_tip_inv_.solveInverseKinematic(current_joint_cable_attaching_end_coordinates, next_tip_coordinate);
    current_revolute_arm_joint_coordinate_ << next_joint_cable_attaching_end_coordinates.segment(0, 2), h0_;
    current_cable_attaching_coordinate_ = next_joint_cable_attaching_end_coordinates.segment(2, 3);
    current_end_coordinate_ = next_joint_cable_attaching_end_coordinates.segment(5, 3);
    Eigen::VectorXd next_joint_cable_attaching_coordinate(5);
    next_joint_cable_attaching_coordinate = next_joint_cable_attaching_end_coordinates.segment(0, 5);
    current_control_value_ = solver_constrained_control_.solveInverseKinematic(current_control_value_, next_joint_cable_attaching_coordinate, lambda_);
    return current_control_value_;
}

Eigen::Vector3d &AFAM::calculateTipCoordinate(Eigen::Vector4d &next_control_value) {
    //lambda_.setZero();
    Eigen::VectorXd current_joint_cable_attaching_coordinates(5);
    current_joint_cable_attaching_coordinates << current_revolute_arm_joint_coordinate_.segment(0, 2), current_cable_attaching_coordinate_;
    Eigen::VectorXd next_joint_cable_attaching_coordinates(5);
    next_joint_cable_attaching_coordinates = solver_constrained_control_.solveForwardKinematic(next_control_value, current_joint_cable_attaching_coordinates, lambda_);
    current_revolute_arm_joint_coordinate_ << next_joint_cable_attaching_coordinates.segment(0, 2), h0_;
    current_cable_attaching_coordinate_ = next_joint_cable_attaching_coordinates.segment(2, 3);
    Eigen::VectorXd current_end_tip_coordinates(6);
    current_end_tip_coordinates << current_end_coordinate_, current_tip_coordinate_;
    Eigen::VectorXd next_end_tip_coordinates(6);
    next_end_tip_coordinates = solver_unconstrained_tip_.solveForwardKinematic(next_joint_cable_attaching_coordinates, current_end_tip_coordinates);
    current_end_coordinate_ = next_end_tip_coordinates.segment(0, 3);
    current_tip_coordinate_ = next_end_tip_coordinates.segment(3, 3);
    return current_tip_coordinate_;
}

void AFAM::updateTrajectory(const Eigen::VectorXd &tip_coordinate){
    point_msg_.x = tip_coordinate(0)/1000000;
    point_msg_.y = tip_coordinate(1)/1000000;
    point_msg_.z = tip_coordinate(2)/1000000;

    points_.header.stamp = ros::Time::now();
    points_.points.push_back(point_msg_);
    marker_publisher_.publish(points_);

    geometry_msgs::Point line_point_msg_;
    line_point_msg_.x = current_cable_attaching_coordinate_(0)/1000000;
    line_point_msg_.y = current_cable_attaching_coordinate_(1)/1000000;
    line_point_msg_.z = current_cable_attaching_coordinate_(2)/1000000;

    visualization_msgs::Marker line_strip_;
    line_strip_.header.frame_id = "base_link";
    line_strip_.ns = "points_and_lines";
    line_strip_.action = visualization_msgs::Marker::ADD;
    line_strip_.pose.orientation.w = 1.0;
    line_strip_.id = 1;
    line_strip_.scale.x = 0.00005;
    line_strip_.color.r = 1.0;
    line_strip_.color.g = 0.0;
    line_strip_.color.b = 0.0;
    line_strip_.color.a = 1.0;

    points_.type = visualization_msgs::Marker::POINTS;
    line_strip_.type = visualization_msgs::Marker::LINE_STRIP;

    line_strip_.header.stamp = ros::Time::now();
    line_strip_.points.push_back(line_point_msg_);
    marker_publisher_.publish(line_strip_);

    line_point_msg_.x = (current_control_value_(0)+xc1_)/1000000;
    line_point_msg_.y = (current_control_value_(1)+yc1_)/1000000;
    line_point_msg_.z = 0;
    line_strip_.header.stamp = ros::Time::now();
    line_strip_.points.push_back(line_point_msg_);
    marker_publisher_.publish(line_strip_);
}

void AFAM::updateJointState(const Eigen::VectorXd &joint_state){
    state_msg_.position[0] = joint_state(0);
    state_msg_.position[1] = joint_state(1);
    state_msg_.position[2] = joint_state(2);
    state_msg_.position[3] = joint_state(3);
    state_msg_.position[4] = joint_state(4);
    state_msg_.position[5] = joint_state(5);
    state_msg_.header.stamp = ros::Time::now();
    afam_joint_publisher_.publish(state_msg_);
}

void AFAM::updateView(const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &joint_state){
    updateJointState(joint_state);
    updateTrajectory(tip_coordinate);
}

Eigen::Vector4d &AFAM::movingTipToNextCoordinate(Eigen::Vector3d &next_tip_coordinate) {
    Eigen::VectorXd current_joint_cable_attaching_end_coordinates(8);
    current_joint_cable_attaching_end_coordinates << current_revolute_arm_joint_coordinate_.segment(0, 2), current_cable_attaching_coordinate_, current_end_coordinate_;
    Eigen::VectorXd next_joint_cable_attaching_end_coordinates(8);
    next_joint_cable_attaching_end_coordinates = solver_unconstrained_tip_inv_.solveInverseKinematic(current_joint_cable_attaching_end_coordinates, next_tip_coordinate);
    current_revolute_arm_joint_coordinate_ << next_joint_cable_attaching_end_coordinates.segment(0, 2), h0_;
    current_cable_attaching_coordinate_ = next_joint_cable_attaching_end_coordinates.segment(2, 3);
    current_end_coordinate_ = next_joint_cable_attaching_end_coordinates.segment(5, 3);
    Eigen::VectorXd next_joint_cable_attaching_coordinate(5);
    next_joint_cable_attaching_coordinate = next_joint_cable_attaching_end_coordinates.segment(0, 5);
    current_control_value_ = solver_constrained_control_.solveInverseKinematic(current_control_value_, next_joint_cable_attaching_coordinate, lambda_);
    std::cout << "control value " << std::endl;
    std::cout << current_control_value_ << std::endl;
    Eigen::VectorXd next_joint_state(6);
    next_joint_state(0) = current_control_value_(0)/1000000; // Input value unit is micro! Calculation unit should same with measuring unit.
    next_joint_state(1) = current_control_value_(1)/1000000;
    next_joint_state(2) = current_control_value_(2)/1000000;
    next_joint_state(3) = current_control_value_(3)/1000000;
    next_joint_state(4) = -boost::math::constants::pi<double>()/2 + std::acos((current_end_coordinate_(2)-current_cable_attaching_coordinate_(2))/(a0_-b0_));
    next_joint_state(5) = boost::math::constants::pi<double>()/2 - std::acos((current_revolute_arm_joint_coordinate_(1)-current_control_value_(3)+yc2_)/d0_);
    updateJointState(next_joint_state);
    return current_control_value_;
}

Eigen::Vector3d &AFAM::movingTipToNextCoordinate(const Eigen::Vector4d &next_control_value) {
    Eigen::VectorXd current_joint_cable_attaching_coordinates(5);
    current_joint_cable_attaching_coordinates << current_revolute_arm_joint_coordinate_.segment(0, 2), current_cable_attaching_coordinate_;
    Eigen::VectorXd next_joint_cable_attaching_coordinates(5);
    next_joint_cable_attaching_coordinates = solver_constrained_control_.solveForwardKinematic(next_control_value, current_joint_cable_attaching_coordinates, lambda_);
    current_revolute_arm_joint_coordinate_ << next_joint_cable_attaching_coordinates.segment(0, 2), h0_;
    current_cable_attaching_coordinate_ = next_joint_cable_attaching_coordinates.segment(2, 3);
    Eigen::VectorXd current_end_tip_coordinates(6);
    current_end_tip_coordinates << current_end_coordinate_, current_tip_coordinate_;
    Eigen::VectorXd next_end_tip_coordinates(6);
    next_end_tip_coordinates = solver_unconstrained_tip_.solveForwardKinematic(next_joint_cable_attaching_coordinates, current_end_tip_coordinates);
    current_end_coordinate_ = next_end_tip_coordinates.segment(0, 3);
    current_tip_coordinate_ = next_end_tip_coordinates.segment(3, 3);
    Eigen::VectorXd next_joint_state(6);
    current_control_value_ = next_control_value;

    next_joint_state(0) = next_control_value(0)/1000000; // Input value unit is micro! Calculation unit should same with measuring unit.
    next_joint_state(1) = next_control_value(1)/1000000;
    next_joint_state(2) = next_control_value(2)/1000000;
    next_joint_state(3) = next_control_value(3)/1000000;
    next_joint_state(4) = -boost::math::constants::pi<double>()/2 + std::acos((current_end_coordinate_(2)-current_cable_attaching_coordinate_(2))/(a0_-b0_));
    next_joint_state(5) = boost::math::constants::pi<double>()/2 - std::acos((current_revolute_arm_joint_coordinate_(1)-current_control_value_(3)+yc2_)/d0_);
    
    //updateJointState(next_joint_state);
    updateView(current_tip_coordinate_, next_joint_state);
    return current_tip_coordinate_;
}

/*void AFAM::movingTipToNextCoordinate(Eigen::Vector3d &next_tip_coordinate) {
    gradient_solver_inv_.setEquation(std::make_shared<ObjectiveFunctionAFAM>(this, next_tip_coordinate));
    current_control_value_ = gradient_solver_inv_.minimizeObjectiveFunction(current_control_value_);
    Eigen::VectorXd next_joint_state(6);
    next_joint_state(0) = current_control_value_(0)/100000; // Input value unit is micro! Calculation unit should same with measuring unit.
    next_joint_state(1) = current_control_value_(1)/100000;
    next_joint_state(2) = current_control_value_(2)/100000;
    next_joint_state(3) = current_control_value_(3)/100000;
    next_joint_state(4) = -boost::math::constants::pi<double>()/2 + std::acos((current_end_coordinate_(2)-current_cable_attaching_coordinate_(2))/(a0_-b0_));
    next_joint_state(5) = boost::math::constants::pi<double>()/2 - std::acos((current_revolute_arm_joint_coordinate_(1)-current_control_value_(3)+yc2_)/d0_);
    updateView(current_tip_coordinate_, next_joint_state);
    updateJointState(next_joint_state);
}*/

Eigen::Vector3d &AFAM::getCurrentTipCoordinate(){
    return current_tip_coordinate_;
}

std::string AFAM::getName() {
    return afam_name_;
}

void AFAM::setPointColor(double alpha, double r, double g, double b){
    points_.color.a = alpha;
    points_.color.r = r;
    points_.color.g = g;
    points_.color.b = b;
}

void AFAM::resetState() {
    current_control_value_.setZero();

    current_revolute_arm_joint_coordinate_(0) = xc2_;      // xr, yr, zr
    current_revolute_arm_joint_coordinate_(1) = yc2_;
    current_revolute_arm_joint_coordinate_(2) = h0_;

    current_cable_attaching_coordinate_(0) = xc2_+d0_+b0_; // xb, yb, zb
    current_cable_attaching_coordinate_(1) = yc2_;
    current_cable_attaching_coordinate_(2) = h0_;

    current_end_coordinate_(0) = xc2_+d0_+a0_;             // xt, yt, zt
    current_end_coordinate_(1) = yc2_;
    current_end_coordinate_(2) = h0_;

    current_tip_coordinate_(0) = xc2_+d0_+a0_;             // x, y, z
    current_tip_coordinate_(1) = yc2_;
    current_tip_coordinate_(2) = h0_-c0_;

    current_control_value_(0) = 0;
    current_control_value_(1) = 0;
    current_control_value_(2) = 0;
    current_control_value_(3) = 0;

    lambda_.setZero();
    Eigen::VectorXd next_joint_state(6);
    next_joint_state.setZero();
    updateJointState(next_joint_state);
}